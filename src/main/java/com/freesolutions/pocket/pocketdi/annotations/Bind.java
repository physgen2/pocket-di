package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Bind {

    int ORDER_DEFAULT = 0;
    int ORDER_MIN = Integer.MIN_VALUE;
    int ORDER_MAX = Integer.MAX_VALUE;
    int ORDER_EARLY = ORDER_MIN + 100;
    int ORDER_LATE = ORDER_MAX - 100;

    /**
     * Some index which can be used to determine start / stop ordering of instances.
     * This value will be available after injection by binding key.
     */
    int order() default ORDER_DEFAULT;
}
