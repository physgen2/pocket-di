package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 * <p/>
 * Indicates that existence of binding is not mandatory for parameter.
 * <p/>
 * Note that all parameters are mandatory by default, and if binding
 * is not exist, then {@link com.freesolutions.pocket.pocketdi.exception.DIException} will be thrown.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Nonmandatory {
    //nothing
}
