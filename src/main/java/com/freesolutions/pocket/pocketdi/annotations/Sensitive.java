package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 * <p/>
 * Indicates that binding & related value/instance, that will be injected to parameter, has sensitive nature.
 * <p/>
 * If value is sensitive, then DI will hide it during any output where necessary.
 * <p/>
 * Note that all values are not sensitive by default.
 * <p/>
 * Also note, that sensitive status is connected to binding & related value/instance, but not
 * to parameter itself, that marked by this annotation. If there exist several places of inject
 * of same binding, and at least one marked as sentitive, then binding & related value/instance
 * became be sensitive.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Sensitive {
    //nothing
}
