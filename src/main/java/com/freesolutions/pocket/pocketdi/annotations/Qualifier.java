package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
public @interface Qualifier {

    /**
     * Value of qualifier.
     */
    String value();
}
