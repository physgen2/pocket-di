package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 * <p/>
 * Only basic types can be injected:
 * <p>
 * {@code String},
 * {@code Integer}, {@code Long}, {@code Float}, {@code Double}, {@code Boolean}, {@code Byte}, {@code Short},
 * {@code int}, {@code long}, {@code float}, {@code double}, {@code boolean}, {@code byte}, {@code short}
 * </p>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Envar {

    /**
     * Required environment variable name.
     */
    String value();
}
