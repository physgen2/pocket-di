package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Manual {
    //nothing
}
