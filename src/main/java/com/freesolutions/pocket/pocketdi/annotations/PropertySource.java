package com.freesolutions.pocket.pocketdi.annotations;

import java.lang.annotation.*;

/**
 * @author Stanislau Mirzayeu
 *
 * If no one @PropertySource annotation specified in injector (in application),
 * then default behaviour works like single annotation:
 * > @PropertySource(value = "sp:application.properties | wd:application.properties | cp:application.properties", mandatory = false)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(PropertySources.class)
public @interface PropertySource {

    /**
     * Alternative sources of properties, provided as string.
     * <p>
     * Value must look like "source1 | source2 | source3 | ...",
     * where "sourceX" is particular source, which will be tried in order of enumeration.
     * First successful source will be used, remaining - ignored.
     * <p>
     * Source must look like "kind:path".
     * - "kind" is one of "sp"- system property, "wd" - working directory, "cp" - classpath, "fs" - specified path in filesystem
     * - "path" is path to properties source, according to specified kind.
     * <p>
     * Example: "sp:props | wd:config/app.properties | fs:/opt/target/app.properties | cp:default.properties".
     */
    String value();

    /**
     * Flag indicates if at least one of alternative property sources, specified in {@link #value}, must be present.
     * If all alternative sources will be failed, then further work will be failed in case of "mandatory" is true,
     * or continued in case of "mandatory" is false.
     * <p>
     * True by default.
     */
    boolean mandatory() default true;
}

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface PropertySources {
    PropertySource[] value();
}
