package com.freesolutions.pocket.pocketdi.internal;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.InstanceAdjuster;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import com.freesolutions.pocket.pocketdi.internal.data.BindingArgument;
import com.freesolutions.pocket.pocketdi.internal.data.BindingCommand;
import com.freesolutions.pocket.pocketdi.internal.data.BindingDescriptor;
import com.freesolutions.pocket.pocketdi.internal.data.BindingParameter;
import com.freesolutions.pocket.pocketdi.utils.LazyWrapping;
import com.freesolutions.pocket.pocketdi.utils.Pair;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.*;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class BindingProcessor {

    @Nonnull
    public static Map<BindingKey, Object> process(
        @Nonnull List<BindingCommand> commands,
        @Nonnull Map<BindingKey, Class<?>> lazies,
        @Nonnull Set<BindingKey> sensitiveKeys,
        @Nonnull List<InstanceAdjuster> instanceAdjusters,
        @Nonnull Map<BindingKey, Object> inOutInstances
    ) {
        checkArgument(
            commands.stream()
                .map(x -> x.bindingDescriptor.ownerKey)
                .collect(Collectors.toSet())
                .containsAll(lazies.keySet())
        );

        //create lazies
        Map<BindingKey, LazyWrapping.LazyResult> lazyResults = lazies.entrySet().stream()
            .map(Pair::of)
            .map(x -> x.mapR(lazyClass -> LazyWrapping.createLazy(lazyClass, x.left)))
            .collect(Collectors.toMap(
                x -> x.left, //ownerKey
                x -> x.right //lazyResult
            ));

        //add proxy instances as temporary
        lazyResults.forEach((ownerKey, lazyResult) ->
            inOutInstances.put(ownerKey, lazyResult.proxy)
        );

        //apply commands
        Map<BindingKey, Object> result = new LinkedHashMap<>();
        for (BindingCommand command : commands) {
            BindingKey ownerKey = command.bindingDescriptor.ownerKey;

            //build arguments
            List<BindingArgument> arguments = buildArguments(
                command.bindingDescriptor,
                command.argumentKeys,
                inOutInstances,
                sensitiveKeys
            );

            //create instance
            Object instance = command.bindingDescriptor.createInstance(arguments);

            //apply instance adjusters if allowed
            if (!command.bindingDescriptor.adjustingDisabled) {
                for (var instanceAdjuster : instanceAdjusters) {
                    instance = adjustInstance(ownerKey, instance, instanceAdjuster);
                }
            }

            //post-process lazy if present
            LazyWrapping.LazyResult lazyResult = lazyResults.remove(ownerKey);
            if (lazyResult != null) {
                lazyResult.setter.accept(instance);
            }

            //add
            inOutInstances.put(ownerKey, instance); //or replace lazies, that probably present before
            result.put(ownerKey, instance);
        }
        checkState(lazyResults.isEmpty());

        //result
        return result;
    }

    @Nonnull
    private static List<BindingArgument> buildArguments(
        @Nonnull BindingDescriptor bindingDescriptor,
        @Nonnull List<Optional<BindingKey>> argumentKeys,
        @Nonnull Map<BindingKey, Object> instances,
        @Nonnull Set<BindingKey> sensitiveKeys
    ) {
        int argumentCount = argumentKeys.size();
        checkArgument(bindingDescriptor.parameters.size() == argumentCount);
        if (argumentCount == 0) { //fast case
            return Collections.emptyList();
        }
        List<BindingArgument> result = new ArrayList<>(argumentCount);
        for (int i = 0; i < argumentCount; i++) {
            Optional<BindingKey> argumentKey = argumentKeys.get(i);
            BindingParameter parameter = bindingDescriptor.parameters.get(i);
            BindingArgument argument = buildArgument(
                bindingDescriptor.ownerKey,
                argumentKey,
                parameter,
                instances,
                sensitiveKeys
            );
            result.add(argument);
        }
        return result;
    }

    @Nonnull
    private static BindingArgument buildArgument(
        @Nonnull BindingKey ownerKey,
        @Nonnull Optional<BindingKey> optArgumentKey,
        @Nonnull BindingParameter parameter,
        @Nonnull Map<BindingKey, Object> instances,
        @Nonnull Set<BindingKey> sensitiveKeys
    ) {
        boolean sensitive = sensitiveKeys.contains(ownerKey);

        //missing?
        if (optArgumentKey.isEmpty()) {
            return BindingArgument.missing(
                ownerKey,
                parameter,
                sensitive
            );
        }

        //normal
        BindingKey argumentKey = optArgumentKey.get();
        Object value = checkNotNull(instances.get(argumentKey)); //must be present, because processing is applied after planning
        return BindingArgument.normal(
            ownerKey,
            argumentKey,
            parameter,
            value,
            sensitive
        );
    }

    @Nonnull
    private static Object adjustInstance(
        @Nonnull BindingKey ownerKey,
        @Nonnull Object instance,
        @Nonnull InstanceAdjuster instanceAdjuster
    ) {
        try {
            instance = instanceAdjuster.adjust(ownerKey, instance);
            if (instance == null) { //NOTE can't trust external code
                throw new DIException(
                    "No instance (null) returned from instance adjuster" +
                        ", owner binding key: " + ownerKey
                );
            }
            return instance;
        } catch (Throwable t) {
            throw new DIException(
                "Failed to adjust instance" +
                    ", owner binding key: " + ownerKey,
                t
            );
        }
    }
}
