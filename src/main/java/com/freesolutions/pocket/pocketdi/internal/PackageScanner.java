package com.freesolutions.pocket.pocketdi.internal;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkState;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class PackageScanner {

    private static final String CLASS_EXT = ".class";
    private static final String JAR_EXT = ".jar";
    private static final String FILE_PREFIX = "file:";
    private static final char DOT_CHAR = '.';
    private static final char SLASH_CHAR = '/';
    private static final char EXCLAMATION_CHAR = '!';

    /**
     * Safe method for scanning classes of specified package.
     *
     * @param classLoader class loader used to find initial resources
     * @param packageName name of package (full java-style package name) to scan
     * @return set of names of found classes (full java-style class names)
     */
    @Nonnull
    public static Set<String> scanClasses(
        @Nonnull ClassLoader classLoader,
        @Nonnull String packageName
    ) {
        //retrieve resources available to scan
        Enumeration<URL> enumResources;
        try {
            enumResources = classLoader.getResources(packageToPath(packageName));
        } catch (Throwable ignored) {
            return Collections.emptySet();
        }

        //divide resources by unique package dirs & jars (heuristic)
        Set<File> packageDirs = new LinkedHashSet<>();
        Set<File> jarFiles = new LinkedHashSet<>();
        while (enumResources.hasMoreElements()) {
            URL resourceUrl = enumResources.nextElement();
            File jarFile = heuristicDetermineOwnerJarFile(resourceUrl);
            if (jarFile != null) {
                jarFiles.add(jarFile);
            } else {
                packageDirs.add(new File(resourceUrl.getFile()));
            }
        }

        //do scan resources according to their kinds
        Set<String> result = new LinkedHashSet<>();
        for (File packageDir : packageDirs) {
            result.addAll(scanClassesInFS(packageDir, packageName));
        }
        for (File jarFile : jarFiles) {
            result.addAll(scanClassesInJar(jarFile, packageName));
        }

        return result;
    }

    @Nonnull
    private static Set<String> scanClassesInFS(
        @Nonnull File packageDir,
        @Nonnull String packageName
    ) {
        Set<String> result = new LinkedHashSet<>();
        try {
            File[] childs = packageDir.exists() ? packageDir.listFiles() : null;
            if (childs != null) {
                for (File child : childs) {
                    if (child.isDirectory()) {
                        checkState(!child.getName().contains("."));
                        result.addAll(scanClassesInFS(child, classOrPackageName(packageName, child.getName())));
                    } else if (child.getName().endsWith(CLASS_EXT)) {
                        String simpleClassName = child.getName().substring(0, child.getName().length() - CLASS_EXT.length());
                        result.add(classOrPackageName(packageName, simpleClassName));
                    }
                }
            }
        } catch (Throwable t) {
            //nothing
        }
        return result;
    }

    @Nonnull
    private static Set<String> scanClassesInJar(
        @Nonnull File jarFile,
        @Nonnull String packageName
    ) {
        Set<String> result = new LinkedHashSet<>();
        try {
            JarFile jar = new JarFile(jarFile);
            String desiredPackagePathWithSlash = packageToPath(packageName) + SLASH_CHAR;
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                String relativeName = entries.nextElement().getName();
                if (relativeName.endsWith(CLASS_EXT)
                    && relativeName.startsWith(desiredPackagePathWithSlash)
                ) {
                    result.add(relativeName
                        .substring(0, relativeName.length() - CLASS_EXT.length())
                        .replace(SLASH_CHAR, DOT_CHAR)
                    );
                }
            }
            jar.close();
        } catch (Throwable ignored) {
            //nothing
        }
        return result;
    }

    @Nullable
    private static File heuristicDetermineOwnerJarFile(@Nonnull URL resourceUrl) {
        String raw = resourceUrl.getFile();
        int right = raw.indexOf(JAR_EXT + EXCLAMATION_CHAR);
        if (right < 0) {
            return null;
        }
        int left = raw.indexOf(FILE_PREFIX);
        if (left < 0 || left >= right) {
            left = 0;
        } else {
            left += FILE_PREFIX.length();
        }
        right += JAR_EXT.length();
        return new File(raw.substring(left, right));
    }

    @Nonnull
    private static String packageToPath(@Nonnull String packageName) {
        String v = packageName.replace(DOT_CHAR, SLASH_CHAR);
        int l = v.length();
        int i = 0;
        while (i < l && v.charAt(i) == SLASH_CHAR) {
            i++;
        }
        int j = l;
        while (j > i && v.charAt(j - 1) == SLASH_CHAR) {
            j--;
        }
        if (i == 0 && j == l) {
            return v;
        }
        if (i == j) {
            return "";
        }
        return v.substring(i, j);
    }

    @Nonnull
    private static String classOrPackageName(
        @Nonnull String parentPackageName,
        @Nonnull String classOrPackageSimpleName
    ) {
        return parentPackageName.isEmpty() ?
            classOrPackageSimpleName :
            parentPackageName + '.' + classOrPackageSimpleName;
    }
}
