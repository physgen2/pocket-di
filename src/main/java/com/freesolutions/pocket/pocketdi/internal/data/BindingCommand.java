package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.BindingKey;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Optional;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingCommand {

    @Nonnull
    public final BindingDescriptor bindingDescriptor;
    @Nonnull
    public final List<Optional<BindingKey>> argumentKeys; //optional-empty element means missing argument (null)

    public BindingCommand(
        @Nonnull BindingDescriptor bindingDescriptor,
        @Nonnull List<Optional<BindingKey>> argumentKeys
    ) {
        checkArgument(argumentKeys.size() == bindingDescriptor.parameters.size());
        this.bindingDescriptor = bindingDescriptor;
        this.argumentKeys = argumentKeys;
    }
}
