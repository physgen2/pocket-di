package com.freesolutions.pocket.pocketdi.internal;

import com.freesolutions.pocket.pocketdi.exception.DIException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.Reader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
public class PropertiesParser {

    @Nonnull
    public static Map<String, String> parse(@Nonnull Reader reader, boolean singleLineMode) {
        Map<String, String> result = new LinkedHashMap<>();
        Cursor cursor = new Cursor();
        TokenIterator i = new TokenIterator(reader, singleLineMode);
        while (true) {//loop of entries
            try {
                if (!parseEntry(i, singleLineMode, cursor, result)) {
                    break;
                }
            } catch (Throwable t) {
                if (t instanceof DIException) {
                    throw (DIException) t;
                }
                throw new DIException(t.getMessage(), t);
            }
        }
        return result;
    }

    private static boolean parseEntry(
        @Nonnull TokenIterator i,
        boolean singleLineMode,
        @Nonnull Cursor cursor,
        @Nonnull Map<String, String> out
    ) {

        //retrieve first token
        i.setCursor(cursor);
        if (!i.hasNext()) {
            return false;
        }
        Token token = i.next();

        //first token for single line mode
        if (singleLineMode) {
            if (token.type != TokenType.MINUS_MINUS) {
                throw new DIException(cursor + ": entry expected to be started from '--'");
            }
            cursor.advance(token);
            i.setCursor(cursor);
            if (!i.hasNext()) {
                return false;
            }
            token = i.next();
        }

        //skip tabs & spaces
        while (token.type == TokenType.TAB_OR_SPACE) {
            cursor.advance(token);
            i.setCursor(cursor);
            if (!i.hasNext()) {
                token = null;
                break;
            }
            token = i.next();
        }
        if (token == null) {
            return false;
        }

        //comment
        if (token.type == TokenType.SHARP) {
            //skip all until divider
            while (true) {
                cursor.advance(token);
                i.setCursor(cursor);
                if (!i.hasNext()) {
                    token = null;
                    break;
                }
                token = i.next();
                if (token.type == TokenType.DIVIDER) {
                    break;
                }
            }
            if (token != null) {
                cursor.advance(token);
            }
            return token != null;
        }

        //no more process for current entry if divider
        if (token.type == TokenType.DIVIDER) {
            cursor.advance(token);
            return true;
        }

        //key
        StringBuilder keySB = new StringBuilder();
        while (token.type != TokenType.EQ && token.type != TokenType.DIVIDER) {
            keySB.append(token.value);
            cursor.advance(token);
            i.setCursor(cursor);
            if (!i.hasNext()) {
                throw new DIException(cursor + ": unexpected end");
            }
            token = i.next();
        }
        if (token.type != TokenType.EQ) {
            throw new DIException(cursor + ": key is not completed");
        }
        String key = keySB.toString().strip();
        if (key.isEmpty()) {
            throw new DIException(cursor + ": empty key");
        }

        //value
        StringBuilder valueSB = new StringBuilder();
        while (true) {
            cursor.advance(token);
            i.setCursor(cursor);
            if (!i.hasNext()) {
                token = null;
                break;
            }
            token = i.next();
            if (token.type == TokenType.DIVIDER) {
                break;
            }
            valueSB.append(token.value);
        }
        String value = valueSB.toString();

        //append key-value to output
        out.put(key, value);

        if (token != null) {
            cursor.advance(token);
        }
        return token != null;
    }

    private static class Cursor {

        public int iRow;
        public int iCol;

        public void advance(@Nonnull Token token) {
            if (token.dRows != 0) {
                this.iRow += token.dRows;
                this.iCol = token.dCols;
            } else {
                this.iCol += token.dCols;
            }
        }

        @Override
        public String toString() {
            return "(row: " + (iRow + 1) + ", column: " + (iCol + 1) + ")";
        }
    }

    @Immutable
    private enum TokenType {
        LITERAL, //simple literal values
        MINUS_MINUS, //--
        EQ, //=
        SHARP, //#
        TAB_OR_SPACE, //tab or space
        DIVIDER, //divider between entries
    }

    @Immutable
    private static class Token {

        @Nonnull
        public final TokenType type;
        @Nonnull
        public final String value;

        public final int dRows;
        public final int dCols;

        @Nonnull
        public static Token of(@Nonnull TokenType type, char ch, int dRows, int dCols) {
            return new Token(type, Character.toString(ch), dRows, dCols);
        }

        @Nonnull
        public static Token of(@Nonnull TokenType type, @Nonnull String value, int dRows, int dCols) {
            return new Token(type, value, dRows, dCols);
        }

        private Token(@Nonnull TokenType type, @Nonnull String value, int dRows, int dCols) {
            this.type = type;
            this.value = value;
            this.dRows = dRows;
            this.dCols = dCols;
        }
    }

    private static class TokenIterator implements Iterator<Token> {

        private static final String HEX_CHARS = "0123456789abcdefABCDEF";

        @Nonnull
        private final Reader reader;
        private final boolean singleLineMode;

        private boolean readerFinished;
        @Nonnull
        private final char[] buffer = new char[4];//4 - capacity for forward chars view
        private int bufferSize;
        @Nullable
        private Token token;
        private boolean noMoreTokens;
        @Nullable
        private Cursor cursor;

        public TokenIterator(@Nonnull Reader reader, boolean singleLineMode) {
            this.reader = reader;
            this.singleLineMode = singleLineMode;
        }

        @Override
        public boolean hasNext() {
            if (noMoreTokens) {
                return false;
            }
            if (token == null) {
                this.token = readToken();
                if (token == null) {
                    this.noMoreTokens = true;
                }
            }
            return !noMoreTokens;
        }

        @Override
        public Token next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Token result = checkNotNull(token);
            this.token = null;
            return result;
        }

        public void setCursor(@Nonnull Cursor cursor) {
            this.cursor = cursor;
        }

        @Nullable
        private Token readToken() {
            if (!ensureBuf(1)) {
                return null;
            }
            int dCol = 1;//default
            char ch = readBuf();
            if (ch == '=') {
                return Token.of(TokenType.EQ, ch, 0, 1);
            }
            if (ch == '#') {
                return Token.of(TokenType.SHARP, ch, 0, 1);
            }
            if (ch == '\t' || ch == ' ') {
                return Token.of(TokenType.TAB_OR_SPACE, ch, 0, 1);
            }
            if (ch == '\n') {
                return Token.of(TokenType.DIVIDER, ch, 1, 0);
            }
            if (ch == '\r' && ensureBuf(1)) {//\r\n
                char nextCh = readBuf();
                if (nextCh == '\n') {
                    return Token.of(TokenType.DIVIDER, "" + ch + nextCh, 1, 0);
                }
                returnBuf(nextCh);
            }
            if (singleLineMode) {
                if (ch == ',' && ensureBuf(2)) {//, followed by --
                    char nextCh1 = readBuf();
                    char nextCh2 = readBuf();
                    returnBuf(nextCh2);
                    returnBuf(nextCh1);
                    if (nextCh1 == '-' && nextCh2 == '-') {
                        return Token.of(TokenType.DIVIDER, ',', 0, 1);
                    }
                }
                if (ch == '-' && ensureBuf(1)) {//--
                    char nextCh = readBuf();
                    if (nextCh == '-') {
                        return Token.of(TokenType.MINUS_MINUS, "" + ch + nextCh, 0, 2);
                    }
                    returnBuf(nextCh);
                }
            }
            if (ch == '\\') {//primary escaping
                if (!ensureBuf(1)) {
                    throw new DIException(cursor + ": unexpected end after escape char '\\'");
                }
                dCol++;
                ch = readBuf();
                if (ch == '\n') {//\\\n
                    return Token.of(TokenType.LITERAL, "", 1, 0);//empty literal, ok
                }
                if (ch == '\r' && ensureBuf(1)) {//\\\r\n
                    char nextCh = readBuf();
                    if (nextCh == '\n') {
                        return Token.of(TokenType.LITERAL, "", 1, 0);//empty literal, ok
                    }
                    returnBuf(nextCh);
                }
                if (ch == '\\' || ch == '\'' || ch == '"' || ch == '=' || (singleLineMode && (ch == ',' || ch == '-'))) {
                    //simple escaping, stay ch as is
                } else if (ch == 'r') {
                    ch = '\r';
                } else if (ch == 'n') {
                    ch = '\n';
                } else if (ch == 'f') {
                    ch = '\f';
                } else if (ch == 't') {
                    ch = '\t';
                } else if (ch == 'b') {
                    ch = '\b';
                } else if (ch == 'u') {//\\uHHHH
                    if (!ensureBuf(4)) {
                        throw new DIException(cursor + ": unexpected end after unicode char escape sequence '\\u'");
                    }
                    dCol += 4;
                    char h3 = readBuf();
                    char h2 = readBuf();
                    char h1 = readBuf();
                    char h0 = readBuf();
                    if (HEX_CHARS.indexOf(h3) < 0
                        || HEX_CHARS.indexOf(h2) < 0
                        || HEX_CHARS.indexOf(h1) < 0
                        || HEX_CHARS.indexOf(h0) < 0
                    ) {
                        throw new DIException(cursor + ": incorrect character hex code after unicode char escape sequence '\\u'");
                    }
                    ch = (char) Integer.parseInt("" + h3 + h2 + h1 + h0, 16);
                } else {
                    throw new DIException(cursor + ": incorrect char '" + charEscape(ch) + "' after escape char '\\'");
                }
            }
            return Token.of(TokenType.LITERAL, ch, 0, dCol);
        }

        private boolean ensureBuf(int count) {
            checkArgument(count >= 0 && count <= buffer.length);
            while (bufferSize < count) {
                if (readerFinished) {
                    return false;
                }
                int iCh;
                try {
                    iCh = reader.read();
                } catch (Throwable t) {
                    throw new DIException(cursor + ": IO-error", t);
                }
                if (iCh < 0) {
                    this.readerFinished = true;
                    return false;
                }
                char ch = (char) iCh;
                buffer[bufferSize++] = ch;
            }
            return true;
        }

        private char readBuf() {
            checkState(bufferSize > 0);
            char result = buffer[0];
            bufferSize--;
            for (int i = 0; i < bufferSize; i++) {
                buffer[i] = buffer[i + 1];
            }
            return result;
        }

        private void returnBuf(char ch) {
            checkState(bufferSize < buffer.length);
            bufferSize++;
            for (int i = bufferSize - 1; i > 0; i--) {
                buffer[i] = buffer[i - 1];
            }
            buffer[0] = ch;
        }

        private static String charEscape(char ch) {
            switch (ch) {
                case '\\':
                    return "\\\\";
                case '\t':
                    return "\\t";
                case '\b':
                    return "\\b";
                case '\n':
                    return "\\n";
                case '\r':
                    return "\\r";
                case '\f':
                    return "\\f";
                case '\"':
                    return "\\\"";
                default:
                    return Character.toString(ch);
            }
        }
    }
}
