package com.freesolutions.pocket.pocketdi.internal;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.RequiredKey;
import com.freesolutions.pocket.pocketdi.internal.data.*;
import com.freesolutions.pocket.pocketdi.utils.DependencyMesh;
import com.freesolutions.pocket.pocketdi.utils.NonnullOpResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class BindingPlanner {

    @Nonnull
    public static BindingPlan plan(
        @Nonnull Collection<BindingDescriptor> bindingDescriptors,
        @Nonnull Set<BindingKey> instanceKeys
    ) {
        //bindingKeys
        Set<BindingKey> bindingKeys = bindingDescriptors.stream()
            .map(x -> x.ownerKey)
            .collect(Collectors.toSet());

        //instanceKeys and bindingDescriptors must be not intersected
        checkArgument(Collections.disjoint(bindingKeys, instanceKeys));

        //keysMapping: parameterKey -> Optional<argumentKey>
        var oprKeysMapping = resolveKeys(bindingDescriptors, concat(instanceKeys, bindingKeys));
        if (!oprKeysMapping.isOk()) {
            return BindingPlan.failure(oprKeysMapping.error());
        }
        Map<RequiredKey, Optional<BindingKey>> keysMapping = oprKeysMapping.value();

        //NOTE: here all parameterKeys guaranteed can be resolved using keysMapping

        //build dependencyMesh & node mappings
        Map<Integer, BindingDescriptor> node2Binding = new HashMap<>();
        Map<BindingKey, Integer> binding2Node = new HashMap<>();
        DependencyMesh dependencyMesh = new DependencyMesh();
        {
            //nodes: based on bindings
            int nodeId = -1;
            for (BindingDescriptor bindingDescriptor : bindingDescriptors) {
                nodeId++;
                node2Binding.put(nodeId, bindingDescriptor);
                binding2Node.put(bindingDescriptor.ownerKey, nodeId);
                dependencyMesh.ensureNode(nodeId); //mesh node
            }

            //links: reference (argumentKey -> bindingKey) for parameters of bindings
            bindingDescriptors.forEach(bindingDescriptor ->
                bindingDescriptor.parameters.stream()
                    .map(x -> checkNotNull(keysMapping.get(x.parameterKey)))
                    .map(x -> x.orElse(null))
                    .filter(Objects::nonNull)
                    .forEach(argumentKey ->
                        dependencyMesh.ensureLink(
                            checkNotNull(binding2Node.get(argumentKey)),
                            checkNotNull(binding2Node.get(bindingDescriptor.ownerKey))
                        )
                    )
            );
        }

        //allowedLazies
        LinkedHashMap<BindingKey, Class<?>> allowedLazies = bindingDescriptors.stream()
            .filter(BindingDescriptor::isAllowLazy)
            .collect(Collectors.toMap(
                x -> x.ownerKey,
                x -> checkNotNull(x.lazyClass),
                (x, y) -> x,
                LinkedHashMap::new //keep order
            ));

        //allowedLazyNodes
        Set<Integer> allowedLazyNodes = allowedLazies.keySet().stream()
            .map(x -> checkNotNull(binding2Node.get(x)))
            .collect(Collectors.toSet());

        //factorize dependencyMesh
        FactorizationPlan factorizationPlan = factorizeMesh(
            dependencyMesh,
            allowedLazyNodes, //includeWeakestCycleNodeIds
            null //excludeWeakestCycleNodeIds
        );

        //mesh factorization failure?
        if (factorizationPlan.failure) {
            return BindingPlan.failure(
                Collections.singletonList(
                    BindingError.cycle(
                        factorizationPlan.unbrokenCycleNodeIds.stream()
                            .map(x -> checkNotNull(node2Binding.get(x)))
                            .map(x -> x.ownerKey)
                            .collect(Collectors.toList()),
                        new ArrayList<>(allowedLazies.keySet()) //keep order
                    )
                )
            );
        }

        //result lazies
        Map<BindingKey, Class<?>> lazies = factorizationPlan.weakestCycleNodeIds.stream()
            .map(x -> checkNotNull(node2Binding.get(x)))
            .map(x -> x.ownerKey)
            .collect(Collectors.toMap(
                Function.identity(),
                x -> checkNotNull(allowedLazies.get(x))
            ));
        checkState(bindingKeys.containsAll(lazies.keySet()));

        //result commands
        List<BindingCommand> commands = factorizationPlan.factorizationNodeIds.stream()
            .map(x -> checkNotNull(node2Binding.get(x)))
            .map(bindingDescriptor ->
                new BindingCommand(
                    bindingDescriptor,
                    bindingDescriptor.parameters.stream()
                        .map(x -> checkNotNull(keysMapping.get(x.parameterKey)))
                        .collect(Collectors.toList())
                )
            )
            .collect(Collectors.toList());
        checkState(bindingDescriptors.size() == commands.size());
        checkState(
            bindingKeys.equals(
                commands.stream()
                    .map(x -> x.bindingDescriptor.ownerKey)
                    .collect(Collectors.toSet())
            )
        );

        //success
        return BindingPlan.success(lazies, commands);
    }

    @Nonnull
    private static NonnullOpResult<Map<RequiredKey, Optional<BindingKey>>, List<BindingError>> resolveKeys(
        @Nonnull Collection<BindingDescriptor> bindingDescriptors,
        @Nonnull Set<BindingKey> availableKeys
    ) {
        Map<RequiredKey, Optional<BindingKey>> resolvedKeys = new HashMap<>();
        List<BindingError> errors = new ArrayList<>();
        for (BindingDescriptor bindingDescriptor : bindingDescriptors) {
            for (BindingParameter parameter : bindingDescriptor.parameters) {

                //skip if same parameterKey was resolved previously
                boolean wasResolved = resolvedKeys.putIfAbsent(parameter.parameterKey, Optional.empty()) != null;
                if (wasResolved) {
                    continue;
                }

                //match parameter & available bindings
                var matchResult = parameter.parameterKey.singleMatched(availableKeys);

                //several bindings found?
                if (!matchResult.isOk()) {
                    errors.add(
                        BindingError.ambiguous(
                            bindingDescriptor.ownerKey,
                            parameter.parameterKey,
                            matchResult.error()
                        )
                    );
                    continue;
                }

                //resolved binding
                BindingKey argumentKey = matchResult.value();

                //binding not found?
                if (argumentKey == null && parameter.mandatory) {
                    errors.add(
                        BindingError.notFound(
                            bindingDescriptor.ownerKey,
                            parameter.parameterKey
                        )
                    );
                    continue;
                }

                //success, binding determined (found or null)
                resolvedKeys.put(
                    parameter.parameterKey,
                    Optional.ofNullable(argumentKey)
                );
            }
        }
        return errors.isEmpty() ?
            NonnullOpResult.ofOk(resolvedKeys) :
            NonnullOpResult.ofNotOk(errors);
    }

    @Nonnull
    private static <T> Set<T> concat(@Nonnull Set<T> set1, @Nonnull Set<T> set2) {
        Set<T> result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    @Immutable
    private static class FactorizationPlan {

        public final boolean failure;
        @Nonnull
        public final List<Integer> factorizationNodeIds;
        @Nonnull
        public final List<Integer> weakestCycleNodeIds;
        @Nonnull
        public final List<Integer> unbrokenCycleNodeIds; //if failure

        @Nonnull
        public static FactorizationPlan success(
            @Nonnull List<Integer> factorizationNodeIds,
            @Nonnull List<Integer> weakestCycleNodeIds
        ) {
            return new FactorizationPlan(
                false, //failure
                factorizationNodeIds,
                weakestCycleNodeIds,
                Collections.emptyList() //unbrokenCycleNodeIds
            );
        }

        @Nonnull
        public static FactorizationPlan failure(
            @Nonnull List<Integer> unbrokenCycleNodeIds
        ) {
            return new FactorizationPlan(
                true, //failure
                Collections.emptyList(), //factorizationNodeIds
                Collections.emptyList(), //weakestCycleNodeIds
                unbrokenCycleNodeIds
            );
        }

        private FactorizationPlan(
            boolean failure,
            @Nonnull List<Integer> factorizationNodeIds,
            @Nonnull List<Integer> weakestCycleNodeIds,
            @Nonnull List<Integer> unbrokenCycleNodeIds
        ) {
            this.failure = failure;
            this.factorizationNodeIds = factorizationNodeIds;
            this.weakestCycleNodeIds = weakestCycleNodeIds;
            this.unbrokenCycleNodeIds = unbrokenCycleNodeIds;
        }
    }

    private static final int DETERMINE_WEAKEST_CYCLE_NODE_STEPS_PER_NODE_SQ = 100;

    private static FactorizationPlan factorizeMesh(
        @Nonnull DependencyMesh dependencyMesh,
        @Nullable Collection<Integer> includeWeakestCycleNodeIds,
        @Nullable Collection<Integer> excludeWeakestCycleNodeIds
    ) {
        List<Integer> factorizationNodeIds = new ArrayList<>();
        List<Integer> weakestCycleNodeIds = new ArrayList<>();
        List<Integer> unbrokenCycleNodeIds = new ArrayList<>();
        DependencyMesh workingMesh = dependencyMesh.copy();
        while (true) {

            //remove free nodes in forward direction
            workingMesh.removeFreeNodesInForwardDirection(factorizationNodeIds);

            //stop if no more nodes remaining
            if (workingMesh.isEmpty()) {
                return FactorizationPlan.success(
                    factorizationNodeIds,
                    weakestCycleNodeIds
                );
            }

            //NOTE: here at least one cycle is present

            //weakest cycle node
            Integer weakestCycleNodeId = workingMesh.determineWeakestCycleNode(
                includeWeakestCycleNodeIds,
                excludeWeakestCycleNodeIds,
                DETERMINE_WEAKEST_CYCLE_NODE_STEPS_PER_NODE_SQ,
                unbrokenCycleNodeIds
            );
            if (weakestCycleNodeId == null) {
                //failure, no weakest node => no progress => can't remove cycles
                return FactorizationPlan.failure(unbrokenCycleNodeIds);
            }
            weakestCycleNodeIds.add(weakestCycleNodeId);
            ;

            //remove forward links of weakest cycle node to partially break cycles
            workingMesh.removeLinks(weakestCycleNodeId, false);
        }
    }

//    public static void main(String[] args) throws Exception {
//
//        //           7 -------> 8
//        //            ^         |
//        //             \        9 <------ 10
//        //              \       |         ^
//        //               \      |         |
//        //        0 -----> 1 -> 2 ------> 3
//        //               / ^              |
//        //             /   |              |
//        //           /     |              |
//        //          6 ---> 5 <----------- 4
//
//        DependencyMesh dependencyMesh = new DependencyMesh();
//        dependencyMesh.ensureLink(0, 1);
//        dependencyMesh.ensureLink(1, 2);
//        dependencyMesh.ensureLink(2, 3);
//        dependencyMesh.ensureLink(3, 4);
//        dependencyMesh.ensureLink(4, 5);
//        dependencyMesh.ensureLink(5, 1);
//        dependencyMesh.ensureLink(1, 6);
//        dependencyMesh.ensureLink(6, 5);
//        dependencyMesh.ensureLink(1, 7);
//        dependencyMesh.ensureLink(7, 8);
//        dependencyMesh.ensureLink(8, 9);
//        dependencyMesh.ensureLink(9, 2);
//        dependencyMesh.ensureLink(3, 10);
//        dependencyMesh.ensureLink(10, 9);
//
//        FactorizationPlan factorizationPlan = factorizeMesh(
//            dependencyMesh,
//            null, //includeWeakestCycleNodeIds
//            Set.of(1, 2, 3, 5, 9) //excludeWeakestCycleNodeIds
//        );
//        if (factorizationPlan.failure) {
//            System.out.println("failure");
//            System.out.println("unbrokenCycleNodeIds: " + factorizationPlan.unbrokenCycleNodeIds);
//        } else {
//            System.out.println("success");
//            DependencyMesh workingMesh = dependencyMesh.copy();
//            factorizationPlan.weakestCycleNodeIds.forEach(workingMesh::removeNode);
//            System.out.println("dependencyMesh without weakest cycle nodes: " + workingMesh.debugToString());
//            System.out.println("factorizationNodeIds: " + factorizationPlan.factorizationNodeIds);
//            System.out.println("weakestCycleNodeIds: " + factorizationPlan.weakestCycleNodeIds);
//        }
//    }
}
