package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.InstanceProducer;
import com.freesolutions.pocket.pocketdi.core.PropertyValue;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import com.freesolutions.pocket.pocketdi.internal.MetaExtractor;
import com.freesolutions.pocket.pocketdi.utils.LazyWrapping;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingDescriptor {

    @Nonnull
    public final BindingKey ownerKey;
    @Nullable
    public final PropertyValue propertyValue; //must be present for properties
    public final boolean adjustingDisabled; //true => any adjusting of instance after creation (proxies, wrappers, ...) is disabled
    @Nullable
    public final Class<?> lazyClass;
    @Nullable
    public final Function<List<Object>, Object> creator; //dynamic creator, null for properties
    @Nonnull
    public final List<BindingParameter> parameters; //parameters for dynamic creator, empty for properties

    @Nonnull
    public static BindingDescriptor forProperty(
        @Nonnull BindingKey ownerKey,
        @Nonnull PropertyValue propertyValue
    ) {
        checkArgument(ownerKey.property);
        return new BindingDescriptor(
            ownerKey,
            true, //adjustingDisabled
            propertyValue,
            null, //lazyClass
            null, //creator
            Collections.emptyList() //parameters
        );
    }

    @Nonnull
    public static BindingDescriptor forInstanceProducer(
        @Nonnull BindingKey ownerKey,
        @Nonnull InstanceProducer instanceProducer
    ) {
        checkArgument(!ownerKey.property);
        return new BindingDescriptor(
            ownerKey,
            false, //adjustingDisabled
            null, //propertyValue
            null, //lazyClass
            noArguments -> {
                try {
                    return instanceProducer.produce();
                } catch (Throwable t) {
                    throw new DIException(
                        "Failed to retrieve instance" +
                            ", owner binding key: " + ownerKey,
                        t
                    );
                }
            },
            Collections.emptyList() //parameters
        );
    }

    @Nonnull
    public static BindingDescriptor forSingletonClass(
        @Nonnull BindingKey ownerKey,
        @Nonnull Class<?> singletonClass,
        boolean adjustingDisabled
    ) {
        checkArgument(!ownerKey.property);

        MetaExtractor<?> metaExtractor = MetaExtractor.of(singletonClass);
        if (!metaExtractor.hasSinglePublicConstructor()) {
            throw new DIException("Class " + singletonClass + " has no single public constructor");
        }

        //lazyClass
        Class<?> lazyClass = !adjustingDisabled && LazyWrapping.isLazyPossible(singletonClass) ? singletonClass : null;

        return new BindingDescriptor(
            ownerKey,
            adjustingDisabled,
            null, //propertyValue
            lazyClass,
            metaExtractor::createClassInstance,
            metaExtractor.constructorParameters()
        );
    }

    @Nonnull
    public static BindingDescriptor forConfigurationMethod(
        @Nonnull BindingKey ownerKey,
        @Nonnull Method configurationMethod
    ) {
        checkArgument(!ownerKey.property);
        checkArgument(!Modifier.isStatic(configurationMethod.getModifiers()));

        MetaExtractor<?> metaExtractor = MetaExtractor.of(configurationMethod.getDeclaringClass());

        //method parameters
        List<BindingParameter> methodParameters = metaExtractor.methodParameters(configurationMethod);

        //gather all parameters
        List<BindingParameter> parameters = new ArrayList<>(1 + methodParameters.size());
        parameters.add( //add "this" parameter first, see code below
            new BindingParameter(
                metaExtractor.classBindingKey().toRequiredKey(), //configuration itself is singleton and must be available
                metaExtractor.cls(),
                null, //propertyExtension
                false, //sensitive
                true //mandatory
            )
        );
        parameters.addAll(methodParameters);

        //lazyClass
        Class<?> returnClass = configurationMethod.getReturnType();
        Class<?> lazyClass = LazyWrapping.isLazyPossible(returnClass) ? returnClass : null;

        return new BindingDescriptor(
            ownerKey,
            false, //adjustingDisabled
            null, //propertyValue
            lazyClass,
            arguments -> {
                //NOTE: first argument is configuration singleton instance, see code above for keys of arguments
                checkState(!arguments.isEmpty());
                return metaExtractor.createConfigurationMethodInstance(
                    checkNotNull(arguments.get(0)), //method is non-static, see check above
                    configurationMethod,
                    arguments,
                    1,
                    arguments.size() - 1
                );
            },
            parameters
        );
    }

    public boolean isAllowLazy() {
        return lazyClass != null;
    }

    @Nonnull
    public Object createInstance(@Nonnull List<BindingArgument> arguments) {
        if (ownerKey.property) {
            return checkNotNull(propertyValue);
        }
        List<Object> rawArguments;
        try {
            rawArguments = arguments.stream()
                .map(BindingArgument::toRawValue)
                .collect(Collectors.toList());
        } catch (Throwable t) {
            throw new DIException(
                "Failed to prepare arguments for binding creator" +
                    ", owner binding key: " + ownerKey,
                t
            );
        }
        Object instance;
        try {
            instance = checkNotNull(creator).apply(rawArguments);
        } catch (Throwable t) {
            throw new DIException(
                "Failed to create instance by binding creator" +
                    ", owner binding key: " + ownerKey,
                t
            );
        }
        if (instance == null) {
            throw new DIException(
                "No instance (null) returned from binding creator" +
                    ", owner binding key: " + ownerKey
            );
        }
        return instance;
    }

    private BindingDescriptor(
        @Nonnull BindingKey ownerKey,
        boolean adjustingDisabled,
        @Nullable PropertyValue propertyValue,
        @Nullable Class<?> lazyClass,
        @Nullable Function<List<Object>, Object> creator,
        @Nonnull List<BindingParameter> parameters
    ) {
        if (ownerKey.property) {
            checkArgument(adjustingDisabled);
            checkArgument(propertyValue != null);
            checkArgument(lazyClass == null);
            checkArgument(creator == null);
            checkArgument(parameters.isEmpty());
        } else {
            checkArgument(propertyValue == null);
            checkArgument(!adjustingDisabled || lazyClass == null);
            checkArgument(creator != null);
        }
        this.ownerKey = ownerKey;
        this.adjustingDisabled = adjustingDisabled;
        this.propertyValue = propertyValue;
        this.lazyClass = lazyClass;
        this.creator = creator;
        this.parameters = parameters;
    }
}
