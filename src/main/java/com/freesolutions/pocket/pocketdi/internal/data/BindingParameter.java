package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.RequiredKey;
import com.freesolutions.pocket.pocketdi.internal.value.PropertyExtension;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingParameter {

    @Nonnull
    public final RequiredKey parameterKey;
    @Nonnull
    public final Class<?> parameterClass;
    @Nullable
    public PropertyExtension propertyExtension;
    public final boolean sensitive;
    public final boolean mandatory;

    public BindingParameter(
        @Nonnull RequiredKey parameterKey,
        @Nonnull Class<?> parameterClass,
        @Nullable PropertyExtension propertyExtension,
        boolean sensitive,
        boolean mandatory
    ) {
        checkArgument(propertyExtension == null || parameterKey.property);
        this.parameterKey = parameterKey;
        this.parameterClass = parameterClass;
        this.propertyExtension = propertyExtension;
        this.sensitive = sensitive;
        this.mandatory = mandatory;
    }
}
