package com.freesolutions.pocket.pocketdi.internal.value;

import com.freesolutions.pocket.pocketdi.core.PropertyValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class PropertyExtension {

    @Immutable
    public enum Kind {

        ENVIRONMENT_VARIABLE("envar"),
        SYSTEM_PROPERTY("sysprop");

        @Nonnull
        public final String displayName;

        Kind(@Nonnull String displayName) {
            this.displayName = displayName;
        }
    }

    @Nonnull
    public final Kind kind;
    @Nonnull
    public final String name;

    public PropertyExtension(@Nonnull Kind kind, @Nonnull String name) {
        this.kind = kind;
        this.name = name;
    }

    @Nullable
    public PropertyValue externalValueOrNull() {
        switch (kind) {
            case ENVIRONMENT_VARIABLE: {
                return environmentVariableOrNull(name);
            }
            case SYSTEM_PROPERTY: {
                return systemPropertyOrNull(name);
            }
            default:
                throw new IllegalStateException("Unknown kind of property extension: " + kind);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropertyExtension that = (PropertyExtension) o;
        if (kind != that.kind) return false;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        int result = kind.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "<" + kind.displayName + ":" + name + ">";
    }

    @Nullable
    private static PropertyValue environmentVariableOrNull(@Nonnull String name) {
        String raw = System.getenv(name);
        return raw != null ? PropertyValue.ofValue(raw) : null;
    }

    @Nullable
    private static PropertyValue systemPropertyOrNull(@Nonnull String name) {
        String raw = System.getProperty(name);
        return raw != null ? PropertyValue.ofValue(raw) : null;
    }
}
