package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.PropertyValue;
import com.freesolutions.pocket.pocketdi.exception.DIException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingArgument {

    public final boolean missing;
    @Nonnull
    public final BindingKey ownerKey;
    @Nullable
    public final BindingKey argumentKey;
    @Nonnull
    public final BindingParameter parameter;
    @Nullable
    public final Object value;
    public final boolean sensitive; //indicates that value is sensitive

    /**
     * Missing: binding is not found, but parameter is not mandatory, so value is absent (null).
     */
    @Nonnull
    public static BindingArgument missing(
        @Nonnull BindingKey ownerKey,
        @Nonnull BindingParameter parameter,
        boolean sensitive
    ) {
        return new BindingArgument(
            true, //missing
            ownerKey,
            null, //argumentKey
            parameter,
            null, //value
            sensitive
        );
    }

    /**
     * Normal: binding is found, value is provided as expected.
     */
    @Nonnull
    public static BindingArgument normal(
        @Nonnull BindingKey ownerKey,
        @Nonnull BindingKey argumentKey,
        @Nonnull BindingParameter parameter,
        @Nonnull Object value,
        boolean sensitive
    ) {
        return new BindingArgument(
            false, //missing
            ownerKey,
            argumentKey,
            parameter,
            value,
            sensitive
        );
    }

    @Nullable
    public Object toRawValue() {
        if (!(value instanceof PropertyValue)) { //implicit null-check
            return value; //here probably null, ok
        }
        try {
            return checkNotNull(((PropertyValue) value).toRawValueOrNull(parameter.parameterClass));
        } catch (Throwable ignored) {
            //NOTE:
            // Exception is ignored, because we want to avoid potential implicit
            // printing of any information about value, because it can be sensitive.
            // Value is printed correctly below.
            throw new DIException(
                "Property value is failed to cast to raw type" +
                    ", owner binding key: " + ownerKey +
                    ", argument required key: " + parameter.parameterKey +
                    ", value: " + valueToPrint() +
                    ", raw type: " + parameter.parameterClass
            );
        }
    }

    @Nonnull
    public String valueToPrint() {
        //allow print null even if sensitive
        return !sensitive || value == null ? String.valueOf(value) : "***hidden***";
    }

    private BindingArgument(
        boolean missing,
        @Nonnull BindingKey ownerKey,
        @Nullable BindingKey argumentKey,
        @Nonnull BindingParameter parameter,
        @Nullable Object value,
        boolean sensitive
    ) {
        if (missing) {
            checkArgument(argumentKey == null);
            checkArgument(value == null);
        } else {
            checkArgument(argumentKey != null);
            checkArgument(value != null);
        }
        this.missing = missing;
        this.ownerKey = ownerKey;
        this.argumentKey = argumentKey;
        this.parameter = parameter;
        this.value = value;
        this.sensitive = sensitive;
    }
}
