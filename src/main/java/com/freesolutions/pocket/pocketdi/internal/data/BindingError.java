package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.RequiredKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingError {

    @Immutable
    public enum Kind {
        CYCLE,
        NOT_FOUND,
        AMBIGUOUS
    }

    @Nonnull
    public final Kind kind;
    @Nullable
    private final BindingKey ownerKey;
    @Nullable
    private final RequiredKey parameterKey;
    @Nonnull
    private final List<BindingKey> additional1;
    @Nonnull
    private final List<BindingKey> additional2;

    @Nonnull
    public static BindingError cycle(
        @Nonnull List<BindingKey> cycleKeys,
        @Nonnull List<BindingKey> allowedLazyKeys
    ) {
        return new BindingError(
            Kind.CYCLE,
            null, //ownerKey
            null, //parameterKey
            cycleKeys,
            allowedLazyKeys
        );
    }

    @Nonnull
    public static BindingError notFound(
        @Nonnull BindingKey ownerKey,
        @Nonnull RequiredKey parameterKey
    ) {
        return new BindingError(
            Kind.NOT_FOUND,
            ownerKey,
            parameterKey,
            Collections.emptyList(),
            Collections.emptyList()
        );
    }

    @Nonnull
    public static BindingError ambiguous(
        @Nonnull BindingKey ownerKey,
        @Nonnull RequiredKey parameterKey,
        @Nonnull List<BindingKey> foundArgumentKeys
    ) {
        return new BindingError(
            Kind.AMBIGUOUS,
            ownerKey,
            parameterKey,
            foundArgumentKeys,
            Collections.emptyList()
        );
    }

    @Nonnull
    public Kind kind() {
        return kind;
    }

    public String describe() {
        switch (kind) {
            case CYCLE: {
                return " >> at least one cycle is detected during binding planning" +
                    ", than can't be resolved using proxy & lazy instantiation" +
                    ", next bindings were detected as cycle core: " + additional1 +
                    ", next bindings were allowed to be lazy: " + additional2;
            }
            case NOT_FOUND: {
                return " >> binding not found for " + parameterKey +
                    " as required dependency to bind " + ownerKey;
            }
            case AMBIGUOUS: {
                return " >> several bindings found for " + parameterKey +
                    " as required dependency to bind " + ownerKey +
                    ", found bindings: " + additional1;
            }
            default:
                throw new IllegalStateException("must be unreachable");
        }
    }

    private BindingError(
        @Nonnull Kind kind,
        @Nullable BindingKey ownerKey,
        @Nullable RequiredKey parameterKey,
        @Nonnull List<BindingKey> additional1,
        @Nonnull List<BindingKey> additional2
    ) {
        switch (kind) {
            case CYCLE: {
                checkArgument(ownerKey == null);
                checkArgument(parameterKey == null);
                checkArgument(!additional1.isEmpty());
                break;
            }
            case NOT_FOUND: {
                checkArgument(ownerKey != null);
                checkArgument(parameterKey != null);
                checkArgument(additional1.isEmpty());
                checkArgument(additional2.isEmpty());
                break;
            }
            case AMBIGUOUS: {
                checkArgument(ownerKey != null);
                checkArgument(parameterKey != null);
                checkArgument(additional1.size() != 1);
                checkArgument(additional2.isEmpty());
                break;
            }
            default:
                throw new IllegalStateException("must be unreachable");
        }
        this.kind = kind;
        this.ownerKey = ownerKey;
        this.parameterKey = parameterKey;
        this.additional1 = additional1;
        this.additional2 = additional2;
    }
}
