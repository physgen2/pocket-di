package com.freesolutions.pocket.pocketdi.internal;

import com.freesolutions.pocket.pocketdi.annotations.*;
import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.PropertyValue;
import com.freesolutions.pocket.pocketdi.core.RequiredKey;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import com.freesolutions.pocket.pocketdi.internal.data.BindingParameter;
import com.freesolutions.pocket.pocketdi.internal.value.PropertyExtension;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class MetaExtractor<T> {

    private static final ConcurrentMap<Class<?>, MetaExtractor<?>> CACHE = new ConcurrentHashMap<>();

    @Nonnull
    private final Class<T> cls;
    @Nullable
    private final Constructor<T> singlePublicConstructor;

    @Nullable
    private BindingKey classBindingKey; //non-volatile cache
    @Nonnull
    private final ConcurrentMap<Method, BindingKey> methodBindingKeys = new ConcurrentHashMap<>();
    @Nullable
    private List<BindingParameter> constructorParameters; //non-volatile cache
    @Nonnull
    private final ConcurrentMap<Method, List<BindingParameter>> methodParameters = new ConcurrentHashMap<>();

    @Nonnull
    public static <T> MetaExtractor<T> of(@Nonnull Class<T> cls) {
        @SuppressWarnings("unchecked")
        MetaExtractor<T> result = (MetaExtractor<T>) CACHE.computeIfAbsent(cls, MetaExtractor::new);
        return result;
    }

    public Class<T> cls() {
        return cls;
    }

    public boolean hasSinglePublicConstructor() {
        return singlePublicConstructor != null;
    }

    @Nonnull
    public BindingKey classBindingKey() {

        //return result if already calculated
        if (classBindingKey != null) {
            return classBindingKey;
        }

        //name
        Named[] nameds = cls.getAnnotationsByType(Named.class);
        if (nameds.length > 1) {
            throw new DIException("Class " + cls + " has more than one @Named");
        }
        String name = null;
        if (nameds.length > 0) {
            name = nameds[0].value();
            if (name.isEmpty()) {
                throw new DIException("Empty name is present in @Named for class " + cls);
            }
        }

        //qualifier
        Qualifier[] qualifiers = cls.getAnnotationsByType(Qualifier.class);
        if (qualifiers.length > 1) {
            throw new DIException("Class " + cls + " has more than one @Qualifier");
        }
        String qualifier = null;
        if (qualifiers.length > 0) {
            qualifier = qualifiers[0].value();
            if (qualifier.isEmpty()) {
                throw new DIException("Empty qualifier is present in @Qualifier for class " + cls);
            }
        }

        //calculated result
        return this.classBindingKey = BindingKey.ofFull(false, name, cls, qualifier);
    }

    @Nonnull
    public BindingKey methodBindingKey(@Nonnull Method method) {

        //result if already calculated
        BindingKey result = methodBindingKeys.get(method);
        if (result != null) {
            return result;
        }

        //check method
        if (!isCorrespondMethod(method)) {
            throw new DIException("Method " + method + " doesn't correspond to class " + cls);
        }
        if (Modifier.isStatic(method.getModifiers())) {
            throw new DIException("Method " + method + " is static and can't be used for binding in configuration " + cls);
        }
        if (!Modifier.isPublic(method.getModifiers())) {
            throw new DIException("Method " + method + " is not public and can't be used for binding in configuration " + cls);
        }
        if (method.getGenericReturnType().equals(Void.TYPE) || method.getGenericReturnType().equals(void.class)) {
            throw new DIException("Method " + method + " has no return type (void) and can't be used for binding in configuration " + cls);
        }

        //name
        String name = null;
        Named[] nameds = method.getAnnotationsByType(Named.class);
        if (nameds.length > 1) {
            throw new DIException("Method " + method + " has more than one @Named for binding in configuration " + cls);
        }
        if (nameds.length > 0) {
            name = nameds[0].value();
            if (name.isEmpty()) {
                throw new DIException("Empty @Named is present for method " + method + " for binding in configuration " + cls);
            }
        }

        //type
        Type type = checkNotNull(method.getGenericReturnType());

        //qualifier
        Qualifier[] qualifiers = method.getAnnotationsByType(Qualifier.class);
        if (qualifiers.length > 1) {
            throw new DIException("Method " + method + " has more than one @Qualifier for binding in configuration " + cls);
        }
        String qualifier = null;
        if (qualifiers.length > 0) {
            qualifier = qualifiers[0].value();
            if (qualifier.isEmpty()) {
                throw new DIException("Empty @Qualifier is present for method " + method + " for binding in configuration " + cls);
            }
        }

        //calculated result
        result = BindingKey.ofFull(false, name, type, qualifier);
        BindingKey tmp = methodBindingKeys.putIfAbsent(method, result);
        if (tmp != null) {
            result = tmp;
        }
        return result;
    }

    @Nonnull
    public List<BindingParameter> constructorParameters() {

        //result if already calculated
        if (constructorParameters != null) {
            return constructorParameters;
        }

        //check single public constructor
        if (singlePublicConstructor == null) {
            throw new DIException("Class " + cls + " has no single public constructor");
        }

        return this.constructorParameters =
            Arrays.stream(singlePublicConstructor.getParameters())
                .map(x -> toParameter(x, "constructor of class " + cls))
                .collect(Collectors.toUnmodifiableList());
    }

    @Nonnull
    public List<BindingParameter> methodParameters(@Nonnull Method method) {

        //result if already calculated
        List<BindingParameter> result = methodParameters.get(method);
        if (result != null) {
            return result;
        }

        //check method
        if (!isCorrespondMethod(method)) {
            throw new DIException("Method " + method + " doesn't correspond to class " + cls);
        }

        //calculated result
        result = Arrays.stream(method.getParameters())
            .map(x -> toParameter(x, "method " + method + " of class " + cls))
            .collect(Collectors.toUnmodifiableList());
        List<BindingParameter> tmp = methodParameters.putIfAbsent(method, result);
        if (tmp != null) {
            result = tmp;
        }

        return result;
    }

    @Nonnull
    public T createClassInstance(@Nonnull List<Object> constructorArgs) {
        return createClassInstance(constructorArgs, 0, constructorArgs.size());
    }

    @Nonnull
    public T createClassInstance(@Nonnull List<Object> constructorArgs, int argsFrom, int argsCount) {

        //check arguments
        if (argsFrom < 0 || argsFrom > constructorArgs.size()) {
            throw new DIException("argsFrom " + argsFrom + " is out of range [0, " + constructorArgs.size() + "]");
        }
        if (argsCount < 0 || argsFrom + argsCount > constructorArgs.size()) {
            throw new DIException("argsCount " + argsCount + " is out of range [0, " + (constructorArgs.size() - argsFrom) + "]");
        }

        //check single public constructor
        if (singlePublicConstructor == null) {
            throw new DIException("Class " + cls + " has no single public constructor");
        }

        //check parameters count
        if (singlePublicConstructor.getParameterCount() != argsCount) {
            throw new DIException(
                "Can't instantiate " + cls + ":" +
                    " argument count (" + argsCount + ")" +
                    " does not match parameters count (" + singlePublicConstructor.getParameterCount() + ")"
            );
        }

        //invoke
        try {
            return singlePublicConstructor.newInstance(toArrayWithoutBordersCheck(constructorArgs, argsFrom, argsCount));
        } catch (Throwable t) {
            throw new DIException(
                "Error while instantiation of " + cls +
                    ", args: " + constructorArgs.subList(argsFrom, argsFrom + argsCount),
                t
            );
        }
    }

    @Nonnull
    public Object createConfigurationMethodInstance(
        @Nonnull Object configuration,
        @Nonnull Method method,
        @Nonnull List<Object> args
    ) {
        return createConfigurationMethodInstance(configuration, method, args, 0, args.size());
    }

    @Nonnull
    public Object createConfigurationMethodInstance(
        @Nonnull Object configuration,
        @Nonnull Method method,
        @Nonnull List<Object> methodArgs,
        int argsFrom,
        int argsCount
    ) {
        //check arguments
        if (!isCorrespondInstance(configuration)) {
            throw new DIException("Configuration class " + configuration.getClass() + " doesn't correspond to class " + cls);
        }
        if (!isCorrespondMethod(method)) {
            throw new DIException("Method " + method + " doesn't correspond to class " + cls);
        }
        if (argsFrom < 0 || argsFrom > methodArgs.size()) {
            throw new DIException("Parameter argsFrom " + argsFrom + " is out of range [0, " + methodArgs.size() + "]");
        }
        if (argsCount < 0 || argsFrom + argsCount > methodArgs.size()) {
            throw new DIException("Parameter argsCount " + argsCount + " is out of range [0, " + (methodArgs.size() - argsFrom) + "]");
        }

        //check parameters count
        if (method.getParameterCount() != argsCount) {
            throw new DIException(
                "Can't instantiate binding with method " + method + ":" +
                    " argument count (" + argsCount + ")" +
                    " does not match parameters count (" + method.getParameterCount() + ")"
            );
        }

        //invoke
        try {
            return method.invoke(configuration, toArrayWithoutBordersCheck(methodArgs, argsFrom, argsCount));
        } catch (Throwable t) {
            throw new DIException(
                "Error while instantiation of binding with method " + method + " (argsCount: " + argsCount + ")",
                t
            );
        }
    }

    private MetaExtractor(@Nonnull Class<T> cls) {
        this.cls = cls;
        this.singlePublicConstructor = singlePublicConstructorOrNull(cls);
    }

    @Nonnull
    private BindingParameter toParameter(@Nonnull Parameter param, @Nonnull String where) {

        //property
        Property[] properties = param.getAnnotationsByType(Property.class);
        if (properties.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Property, see " + where);
        }
        boolean property = properties.length > 0;
        String name = null;
        if (property) {
            name = properties[0].value();
            if (name.isEmpty()) {
                throw new DIException("Empty name is present in @Property for parameter " + param.getName() + ", see " + where);
            }
        }

        //envar | sysprop
        Envar[] envars = param.getAnnotationsByType(Envar.class);
        if (envars.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Envar, see " + where);
        }
        Sysprop[] sysprops = param.getAnnotationsByType(Sysprop.class);
        if (sysprops.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Sysprop, see " + where);
        }
        PropertyExtension propertyExtension = null;
        boolean envar = envars.length > 0;
        boolean sysprop = sysprops.length > 0;
        if (envar) {
            if (!property) {
                throw new DIException("Redundant @Envar when @Property is not specified for parameter " + param.getName() + ", see " + where);
            }
            if (sysprop) {
                throw new DIException("Redundant @Sysprop when @Envar already specified for parameter " + param.getName() + ", see " + where);
            }
            String extName = envars[0].value();
            if (extName.isEmpty()) {
                throw new DIException("Empty name is present in @Envar for parameter " + param.getName() + ", see " + where);
            }
            propertyExtension = new PropertyExtension(
                PropertyExtension.Kind.ENVIRONMENT_VARIABLE,
                extName
            );
        }
        if (sysprop) {
            if (!property) {
                throw new DIException("Redundant @Sysprop when @Property is not specified for parameter " + param.getName() + ", see " + where);
            }
            String extName = sysprops[0].value();
            if (extName.isEmpty()) {
                throw new DIException("Empty name is present in @Sysprop for parameter " + param.getName() + ", see " + where);
            }
            propertyExtension = new PropertyExtension(
                PropertyExtension.Kind.SYSTEM_PROPERTY,
                extName
            );
        }

        //name
        Named[] nameds = param.getAnnotationsByType(Named.class);
        if (nameds.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Named, see " + where);
        }
        if (nameds.length > 0) {
            if (property) {
                throw new DIException("Redundant @Named when @Property already specified for parameter " + param.getName() + ", see " + where);
            }
            name = nameds[0].value();
            if (name.isEmpty()) {
                throw new DIException("Empty @Named is present for parameter " + param.getName() + ", see " + where);
            }
        }

        //type
        Type type = null;
        if (property) {
            if (!PropertyValue.isPropertyType(param.getType())) {
                throw new DIException("Type of parameter " + param.getName() + " is not compatible with property, see " + where);
            }
        } else {
            type = checkNotNull(param.getParameterizedType());
        }

        //qualifier
        Qualifier[] qualifiers = param.getAnnotationsByType(Qualifier.class);
        if (qualifiers.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Qualifier, see " + where);
        }
        String qualifier = null;
        if (qualifiers.length > 0) {
            if (property) {
                throw new DIException("Redundant @Qualifier when @Property already specified for parameter " + param.getName() + ", see " + where);
            }
            qualifier = qualifiers[0].value();
            if (qualifier.isEmpty()) {
                throw new DIException("Empty @Qualifier is present for parameter " + param.getName() + ", see " + where);
            }
        }

        //sensitive
        Sensitive[] sensitives = param.getAnnotationsByType(Sensitive.class);
        if (sensitives.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Sensitive, see " + where);
        }
        boolean sensitive = sensitives.length > 0;

        //mandatory
        Nonmandatory[] nonmandatories = param.getAnnotationsByType(Nonmandatory.class);
        if (nonmandatories.length > 1) {
            throw new DIException("Parameter " + param.getName() + " has more than one @Nonmandatory, see " + where);
        }
        boolean mandatory = nonmandatories.length == 0; //NOTE inverted

        //result
        return new BindingParameter(
            RequiredKey.ofFull(property, name, type, qualifier), //parameterKey
            param.getType(), //parameterClass
            propertyExtension,
            sensitive,
            mandatory
        );
    }

    private boolean isCorrespondInstance(@Nonnull Object instance) {
        return cls.isAssignableFrom(instance.getClass());
    }

    private boolean isCorrespondMethod(@Nonnull Method method) {
        return method.getDeclaringClass().isAssignableFrom(cls);
    }

    @Nullable
    private static <T> Constructor<T> singlePublicConstructorOrNull(@Nonnull Class<T> cls) {
        @SuppressWarnings("unchecked")
        Constructor<T>[] constructors = (Constructor<T>[]) cls.getConstructors();
        return constructors.length == 1 ? constructors[0] : null;
    }

    @Nonnull
    private static Object[] toArrayWithoutBordersCheck(@Nonnull List<Object> list, int from, int count) {
        Object[] result = new Object[count];
        for (int i = 0; i < count; i++) {
            result[i] = list.get(from + i);
        }
        return result;
    }
}
