package com.freesolutions.pocket.pocketdi.internal.data;

import com.freesolutions.pocket.pocketdi.core.BindingKey;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingPlan {

    @Nonnull
    public final Map<BindingKey, Class<?>> lazies;
    @Nonnull
    public final List<BindingCommand> commands;
    @Nonnull
    public final List<BindingError> errors;

    @Nonnull
    public static BindingPlan success(
        @Nonnull Map<BindingKey, Class<?>> lazies,
        @Nonnull List<BindingCommand> commands
    ) {
        return new BindingPlan(lazies, commands, Collections.emptyList());
    }

    @Nonnull
    public static BindingPlan failure(@Nonnull List<BindingError> errors) {
        checkArgument(!errors.isEmpty());
        return new BindingPlan(Collections.emptyMap(), Collections.emptyList(), errors);
    }

    private BindingPlan(
        @Nonnull Map<BindingKey, Class<?>> lazies,
        @Nonnull List<BindingCommand> commands,
        @Nonnull List<BindingError> errors
    ) {
        checkArgument(errors.isEmpty() || (lazies.isEmpty() && commands.isEmpty()));
        this.lazies = lazies;
        this.commands = commands;
        this.errors = errors;
    }
}
