package com.freesolutions.pocket.pocketdi.internal;

import com.freesolutions.pocket.pocketdi.annotations.PropertySource;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author Stanislau Mirzayeu
 */
@ThreadSafe
public class PropertiesExtractor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesExtractor.class);

    private static final String DEFAULT_PROPERTY_SOURCE_COMMAND =
        "sp:application.properties | wd:application.properties | cp:application.properties";

    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final Set<String> SOURCE_KINDS = Set.of("sp", "wd", "cp", "fs");

    @Nonnull
    public static Map<String, String> gatherProperties(@Nonnull PropertySource[] propertySources) {
        if (propertySources.length == 0) {
            return Collections.emptyMap();
        }
        Map<String, String> result = new LinkedHashMap<>();
        List<String> succeeds = new ArrayList<>();
        List<String> errors = new ArrayList<>();
        for (PropertySource propertySource : propertySources) {
            String command = propertySource.value();
            List<String> lErrors = new ArrayList<>();
            String succeed = handleCommandSafe(command, result, lErrors);
            if (propertySource.mandatory() && succeed == null) {
                throw new DIException(
                    "Property source marked as mandatory, but all paths failed" +
                        ", see command: " + command +
                        ", errors: " + lErrors
                );
            }
            if (succeed != null) {
                succeeds.add(succeed);
            }
            errors.addAll(lErrors);
        }
        print(succeeds, errors);
        return result;
    }

    @Nonnull
    public static Map<String, String> gatherPropertiesFromDefaultSource() {
        Map<String, String> result = new LinkedHashMap<>();
        List<String> errors = new ArrayList<>();
        String succeed = handleCommandSafe(DEFAULT_PROPERTY_SOURCE_COMMAND, result, errors);
        print(
            succeed != null ?
                Collections.singletonList(succeed) :
                Collections.emptyList(),
            errors
        );
        return result;
    }

    @Nullable
    public static String handleCommandSafe(
        @Nonnull String command,
        @Nonnull Map<String, String> outProperties,
        @Nonnull List<String> outErrors
    ) {
        String result = null;
        for (String rawAltCommand : command.split("\\|")) {
            String altCommand = rawAltCommand.strip();
            if (altCommand.isEmpty()) {
                continue;
            }
            String[] parts = altCommand.split(":");
            if (parts.length != 2) {
                throw new DIException("Incorrect property source command: " + command);
            }
            String sourceKind = parts[0].strip();
            if (sourceKind.isEmpty()) {
                throw new DIException("Incorrect property source command: " + command);
            }
            String path = parts[1].strip();
            if (path.isEmpty()) {
                throw new DIException("Incorrect property source command: " + command);
            }
            if (!SOURCE_KINDS.contains(sourceKind)) {
                throw new DIException("Incorrect property source command: " + command);
            }
            try {
                switch (sourceKind) {
                    case "sp": {
                        outProperties.putAll(readFromSystemProperty(path));
                        break;
                    }
                    case "wd": {
                        outProperties.putAll(readFromWorkingDirectory(path));
                        break;
                    }
                    case "cp": {
                        outProperties.putAll(readFromClassPath(path));
                        break;
                    }
                    case "fs": {
                        outProperties.putAll(readFromFileSystem(path));
                        break;
                    }
                }
            } catch (Throwable t) {
                outErrors.add(sourceKind + ":" + path + " => " + t.getMessage());
                continue;
            }
            result = sourceKind + ":" + path;
            break; //command was processed successfully, no more alternative tries required
        }
        return result;
    }

    private static void print(@Nonnull List<String> succeeds, @Nonnull List<String> errors) {
        StringBuilder sb = new StringBuilder("\n\n");
        if (!errors.isEmpty()) {
            for (String error : errors) {
                sb.append(">> Properties ignored try: ").append(error).append('\n');
            }
        }
        if (!succeeds.isEmpty()) {
            for (String succeed : succeeds) {
                sb.append(">> Properties succeeded: ").append(succeed).append('\n');
            }
        } else {
            sb.append(">> No succeeded properties\n");
        }
        LOGGER.info(sb.toString());
    }

    @Nonnull
    private static Map<String, String> readFromSystemProperty(@Nonnull String path) {
        String raw = System.getProperty(path);
        if (raw == null) {
            throw new DIException("System property " + path + " is not available");
        }
        try (Reader reader = new StringReader(raw)) {
            return PropertiesParser.parse(reader, true);
        } catch (IOException e) {
            throw new DIException(e);
        }
    }

    @Nonnull
    private static Map<String, String> readFromWorkingDirectory(@Nonnull String path) {
        try (Reader reader = new BufferedReader(
            new InputStreamReader(
                new FileInputStream(
                    new File(new File(System.getProperty("user.dir")), path)
                ),
                CHARSET
            )
        )) {
            return PropertiesParser.parse(reader, false);
        } catch (IOException e) {
            throw new DIException(e);
        }
    }

    @Nonnull
    private static Map<String, String> readFromClassPath(@Nonnull String path) {
        InputStream is = PropertiesExtractor.class.getClassLoader().getResourceAsStream(path);
        if (is == null) {
            throw new DIException("Resource " + path + " is not available");
        }
        try (Reader reader = new BufferedReader(
            new InputStreamReader(is, CHARSET)
        )) {
            return PropertiesParser.parse(reader, false);
        } catch (IOException e) {
            throw new DIException(e);
        }
    }

    @Nonnull
    private static Map<String, String> readFromFileSystem(@Nonnull String path) {
        try (Reader reader = new BufferedReader(
            new InputStreamReader(
                new FileInputStream(path),
                CHARSET
            )
        )) {
            return PropertiesParser.parse(reader, false);
        } catch (IOException e) {
            throw new DIException(e);
        }
    }
}
