package com.freesolutions.pocket.pocketdi.core;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
@FunctionalInterface
public interface InstanceAdjuster {

    @Nonnull
    Object adjust(@Nonnull BindingKey ownerKey, @Nonnull Object instance) throws Exception;
}
