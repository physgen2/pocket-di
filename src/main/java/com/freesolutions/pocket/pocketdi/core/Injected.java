package com.freesolutions.pocket.pocketdi.core;

import com.freesolutions.pocket.pocketdi.internal.value.PropertyExtension;
import com.freesolutions.pocket.pocketdi.utils.NullableOpResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class Injected {

    private final boolean propertySourcesGathered;
    @Nonnull
    private final LinkedHashMap<BindingKey, Object> instances; //instances
    @Nonnull
    private final Map<BindingKey, Integer> orders; //orders of instances
    @Nonnull
    private final Set<BindingKey> sensitiveKeys;
    @Nonnull
    private final Set<RequiredKey> sensitiveParameterKeys;
    @Nonnull
    private final Map<BindingKey, PropertyExtension> propertyExtensions;

    @Nonnull
    public Injector toInjector() {
        return Injector.basedOn(this);
    }

    @Nonnull
    public Map<BindingKey, Object> instances() {
        return Collections.unmodifiableMap(instances);
    }

    @Nonnull
    public Map<BindingKey, Integer> orders() {
        return Collections.unmodifiableMap(orders);
    }

    @Nullable
    public Integer orderExact(@Nonnull BindingKey bindingKey) {
        return orders.get(bindingKey);
    }

    @Nonnull
    public NullableOpResult<Integer, List<BindingKey>> orderRequired(@Nonnull RequiredKey requiredKey) {
        var opr = requiredKey.singleMatched(instances.keySet());
        if (!opr.isOk()) {
            return checkNotNull(NullableOpResult.ofNotOkIfNotOk(opr));
        }
        BindingKey bindingKey = opr.value();
        return NullableOpResult.ofOk(bindingKey != null ? checkNotNull(orders.get(bindingKey)) : null);
    }

    @Nullable
    public Object instanceExact(@Nonnull BindingKey bindingKey) {
        return instances.get(bindingKey);
    }

    @Nullable
    public <T> T instanceExact(@Nonnull BindingKey bindingKey, Class<T> targetClass) {
        Object value = instanceExact(bindingKey);
        if (value == null) {
            return null;
        }
        if (bindingKey.property) {
            checkState(value instanceof PropertyValue);
            T r;
            try {
                r = ((PropertyValue) value).toRawValueOrNull(targetClass);
            } catch (Throwable t) {
                throw new IllegalArgumentException(
                    "Property value can't be casted to requested target class" +
                        ", bindingKey: " + bindingKey +
                        ", value: " + value +
                        ", targetClass: " + targetClass,
                    t
                );
            }
            if (r == null) {
                throw new ClassCastException("Requested target class " + targetClass + " is not property type");
            }
            return r;
        }
        if (!targetClass.isAssignableFrom(value.getClass())) {
            throw new ClassCastException("Requested target class " + targetClass + " is not compatible with actual type of binding " + value.getClass());
        }
        @SuppressWarnings("unchecked")
        T r = (T) value;
        return r;
    }

    @Nonnull
    public NullableOpResult<Object, List<BindingKey>> instanceRequired(@Nonnull RequiredKey requiredKey) {
        var opr = requiredKey.singleMatched(instances.keySet());
        if (!opr.isOk()) {
            return checkNotNull(NullableOpResult.ofNotOkIfNotOk(opr));
        }
        BindingKey bindingKey = opr.value();
        return NullableOpResult.ofOk(bindingKey != null ? checkNotNull(instances.get(bindingKey)) : null);
    }

    @Nonnull
    public <T> NullableOpResult<T, List<BindingKey>> instanceRequired(@Nonnull RequiredKey requiredKey, Class<T> targetClass) {
        var opr = instanceRequired(requiredKey);
        if (!opr.isOk()) {
            return checkNotNull(NullableOpResult.ofNotOkIfNotOk(opr));
        }
        Object value = opr.value();
        if (value == null) {
            return NullableOpResult.ofOkNull();
        }
        if (requiredKey.property) {
            checkState(value instanceof PropertyValue);
            T r;
            try {
                r = ((PropertyValue) value).toRawValueOrNull(targetClass);
            } catch (Throwable t) {
                throw new IllegalArgumentException(
                    "Property value can't be casted to requested target class" +
                        ", requiredKey: " + requiredKey +
                        ", value: " + value +
                        ", targetClass: " + targetClass,
                    t
                );
            }
            if (r == null) {
                throw new ClassCastException("Requested target class " + targetClass + " is not property type");
            }
            return NullableOpResult.ofOk(r);
        }
        if (!targetClass.isAssignableFrom(value.getClass())) {
            throw new ClassCastException("Requested target class " + targetClass + " is not compatible with actual type of binding " + value.getClass());
        }
        @SuppressWarnings("unchecked")
        T r = (T) value;
        return NullableOpResult.ofOk(r);
    }

    private Injected(
        boolean propertySourcesGathered,
        @Nonnull LinkedHashMap<BindingKey, Object> instances,
        @Nonnull Map<BindingKey, Integer> orders,
        @Nonnull Set<BindingKey> sensitiveKeys,
        @Nonnull Set<RequiredKey> sensitiveParameterKeys,
        @Nonnull Map<BindingKey, PropertyExtension> propertyExtensions
    ) {
        this.propertySourcesGathered = propertySourcesGathered;
        this.instances = instances;
        this.orders = orders;
        this.sensitiveKeys = sensitiveKeys;
        this.sensitiveParameterKeys = sensitiveParameterKeys;
        this.propertyExtensions = propertyExtensions;
    }

    @NotThreadSafe
    static class Mutable { //package-protected

        public boolean propertySourcesGathered;
        @Nonnull
        public final LinkedHashMap<BindingKey, Object> instances; //instances
        @Nonnull
        public final Map<BindingKey, Integer> orders; //orders of instances
        @Nonnull
        public final Set<BindingKey> sensitiveKeys;
        @Nonnull
        public final Set<RequiredKey> sensitiveParameterKeys;
        @Nonnull
        public final Map<BindingKey, PropertyExtension> propertyExtensions;

        public Mutable() {
            this.propertySourcesGathered = false;
            this.instances = new LinkedHashMap<>();
            this.orders = new HashMap<>();
            this.sensitiveKeys = new HashSet<>();
            this.sensitiveParameterKeys = new HashSet<>();
            this.propertyExtensions = new HashMap<>();
        }

        public Mutable(@Nonnull Injected injected) {
            this.propertySourcesGathered = injected.propertySourcesGathered;
            this.instances = new LinkedHashMap<>(injected.instances);
            this.orders = new HashMap<>(injected.orders);
            this.sensitiveKeys = new HashSet<>(injected.sensitiveKeys);
            this.sensitiveParameterKeys = new HashSet<>(injected.sensitiveParameterKeys);
            this.propertyExtensions = new HashMap<>(injected.propertyExtensions);
        }

        @Nonnull
        public Injected fix() {

            //checks
            checkArgument(Objects.equals(instances.keySet(), orders.keySet()));
            checkArgument(instances.keySet().containsAll(sensitiveKeys));
            checkArgument( //every sensitiveKey must be matched by at least one sensitiveParameterKey
                sensitiveKeys.stream().allMatch(sensitiveKey ->
                    sensitiveParameterKeys.stream().anyMatch(parameterKey -> parameterKey.match(sensitiveKey))
                )
            );
            checkArgument(propertyExtensions.keySet().stream().allMatch(x -> x.property));

            return new Injected(
                propertySourcesGathered,
                new LinkedHashMap<>(instances),
                new HashMap<>(orders),
                new HashSet<>(sensitiveKeys),
                new HashSet<>(sensitiveParameterKeys),
                new HashMap<>(propertyExtensions)
            );
        }
    }
}
