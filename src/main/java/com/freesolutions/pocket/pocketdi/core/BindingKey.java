package com.freesolutions.pocket.pocketdi.core;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Comparator;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class BindingKey {

    private static final Comparator<BindingKey> PRETTY_ORDERING =
        Comparator.<BindingKey, Boolean>comparing(x -> x.property)
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.name))
            )
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.type != null ? x.type.getTypeName() : null))
            )
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.qualifier))
            );

    public final boolean property;
    @Nullable
    public final String name;
    @Nullable
    public final Type type;
    @Nullable
    public final String qualifier;

    @Nonnull
    public static Comparator<BindingKey> prettyOrdering() {
        return PRETTY_ORDERING;
    }

    @Nonnull
    public static BindingKey ofProperty(@Nonnull String name) {
        return new BindingKey(true, name, null, null);
    }

    @Nonnull
    public static BindingKey ofTypeOnly(@Nonnull Type type) {
        return new BindingKey(false, null, type, null);
    }

    @Nonnull
    public static BindingKey ofTypeQualified(@Nonnull Type type, @Nonnull String qualifier) {
        return new BindingKey(false, null, type, qualifier);
    }

    @Nonnull
    public static BindingKey ofTypeNamed(@Nonnull String name, @Nonnull Type type) {
        return new BindingKey(false, name, type, null);
    }

    @Nonnull
    public static BindingKey ofFull(
        boolean property,
        @Nullable String name,
        @Nullable Type type,
        @Nullable String qualifier
    ) {
        return new BindingKey(property, name, type, qualifier);
    }

    @Nonnull
    public Class<?> javaType() {
        if (type == null) {
            return Object.class;
        }
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        }
        if (type instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) type).getRawType();
        }
        throw new IllegalStateException("must be impossible, see checks in constructor");
    }

    @Nonnull
    public RequiredKey toRequiredKey() {
        return RequiredKey.ofFull(property, name, type, qualifier);
    }

    public boolean clash(@Nonnull BindingKey other) {
        //NOTE code of this method must be symmetric against both keys
        if (property != other.property) {
            return false;
        }
        if (property) {
            return checkNotNull(name).equals(checkNotNull(other.name));
        }
        if (name != null && other.name != null && name.equals(other.name)) {
            return true;
        }
        if (!checkNotNull(type).equals(checkNotNull(other.type))) {
            return false;
        }
        if (qualifier != null && other.qualifier != null && qualifier.equals(other.qualifier)) {
            return true;
        }
        //in cases below keys can hide each other
        if ((name != null && other.name != null) || (qualifier != null && other.qualifier != null)) {
            return false;
        }
        if ((name == null && other.name == null) || (qualifier == null && other.qualifier == null)) {
            return true;
        }
        if ((name != null && qualifier == null) || (other.name != null && other.qualifier == null)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BindingKey that = (BindingKey) o;
        if (property != that.property) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return qualifier != null ? qualifier.equals(that.qualifier) : that.qualifier == null;
    }

    @Override
    public int hashCode() {
        int result = (property ? 1 : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (qualifier != null ? qualifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        if (property) {
            return "<prop:" + name + ">";
        }
        return "<bean" +
            (name != null ? ":" + name : "") +
            "|" + checkNotNull(type).getTypeName() +
            (qualifier != null ? "(" + qualifier + ")" : "") +
            ">";
    }

    private BindingKey(
        boolean property,
        @Nullable String name,
        @Nullable Type type,
        @Nullable String qualifier
    ) {
        if (property) {
            checkArgument(name != null, "name must be present");
            checkArgument(type == null, "type must be not specified");
            checkArgument(qualifier == null, "qualifier must be not specified");
        }
        if (!property) {
            checkArgument(type != null, "type must be specified");
        }
        if (name != null) {
            checkArgument(isNameValid(name), "name \"%s\" is invalid", name);
        }
        if (type != null) {
            checkArgument(
                type instanceof Class<?> || type instanceof ParameterizedType,
                "type must be one of Class<?> or ParameterizedType"
            );
        }
        if (qualifier != null) {
            checkArgument(isQualifierValid(qualifier), "qualifier \"%s\" is invalid", qualifier);
        }
        this.property = property;
        this.name = name;
        this.type = type;
        this.qualifier = qualifier;
    }

    private static boolean isNameValid(@Nonnull String name) {
        return !name.isEmpty()
            && name.charAt(0) != '.'
            && name.charAt(name.length() - 1) != '.'
            && name.matches("[a-zA-Z0-9._-]+");
    }

    private static boolean isQualifierValid(@Nonnull String qualifier) {
        return !qualifier.isEmpty()
            && qualifier.charAt(0) != '.'
            && qualifier.charAt(qualifier.length() - 1) != '.'
            && qualifier.matches("[a-zA-Z0-9._-]+");
    }
}
