package com.freesolutions.pocket.pocketdi.core;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
@FunctionalInterface
public interface InstanceProducer {

    @Nonnull
    Object produce() throws Exception;
}
