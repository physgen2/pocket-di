package com.freesolutions.pocket.pocketdi.core;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class PropertyValue {

    @Nonnull
    public final String value;
    @Nonnull
    public final String pure;

    public static boolean isPropertyType(@Nonnull Class<?> type) {
        return type == String.class
            || type == Integer.class || type == int.class
            || type == Long.class || type == long.class
            || type == Float.class || type == float.class
            || type == Double.class || type == double.class
            || type == Boolean.class || type == boolean.class
            || type == Byte.class || type == byte.class
            || type == Short.class || type == short.class;
    }

    @Nonnull
    public static PropertyValue ofValue(String value) {
        return new PropertyValue(value);
    }

    @Nullable
    public static PropertyValue ofRawValueOrNull(@Nonnull Object rawValue) {
        if (rawValue instanceof PropertyValue) {
            return (PropertyValue) rawValue;
        }
        if (rawValue instanceof String) {
            return new PropertyValue((String) rawValue);
        }
        if (rawValue instanceof Integer) {
            return new PropertyValue(Integer.toString((Integer) rawValue));
        }
        if (rawValue instanceof Long) {
            return new PropertyValue(Long.toString((Long) rawValue));
        }
        if (rawValue instanceof Float) {
            return new PropertyValue(Float.toString((Float) rawValue));
        }
        if (rawValue instanceof Double) {
            return new PropertyValue(Double.toString((Double) rawValue));
        }
        if (rawValue instanceof Boolean) {
            return new PropertyValue(Boolean.toString((Boolean) rawValue));
        }
        if (rawValue instanceof Byte) {
            return new PropertyValue(Byte.toString((Byte) rawValue));
        }
        if (rawValue instanceof Short) {
            return new PropertyValue(Short.toString((Short) rawValue));
        }
        return null;
    }

    @Nullable
    public <T> T toRawValueOrNull(Class<T> valueType) {
        if (valueType == PropertyValue.class) {
            @SuppressWarnings("unchecked")
            T result = (T) this;
            return result;
        }
        if (valueType == String.class) {
            @SuppressWarnings("unchecked")
            T result = (T) value;
            return result;
        }
        if (valueType == Integer.class || valueType == int.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Integer.valueOf(pure);
            return result;
        }
        if (valueType == Long.class || valueType == long.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Long.valueOf(pure);
            return result;
        }
        if (valueType == Float.class || valueType == float.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Float.valueOf(pure);
            return result;
        }
        if (valueType == Double.class || valueType == double.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Double.valueOf(pure);
            return result;
        }
        if (valueType == Boolean.class || valueType == boolean.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Boolean.valueOf(pure);
            return result;
        }
        if (valueType == Byte.class || valueType == byte.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Byte.valueOf(pure);
            return result;
        }
        if (valueType == Short.class || valueType == short.class) {
            @SuppressWarnings("unchecked")
            T result = (T) Short.valueOf(pure);
            return result;
        }
        return null;
    }

    @Override
    public String toString() {
        return "PropertyValue[\"" + javaEscape(value) + "\" => \"" + javaEscape(pure) + "\"]";
    }

    public static String javaEscape(String s) {
        return s.replace("\\", "\\\\")
            .replace("\t", "\\t")
            .replace("\b", "\\b")
            .replace("\n", "\\n")
            .replace("\r", "\\r")
            .replace("\f", "\\f")
            .replace("\"", "\\\"");
    }

    private PropertyValue(@Nonnull String value) {
        this.value = value;
        this.pure = value.strip();
    }
}
