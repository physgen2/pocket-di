package com.freesolutions.pocket.pocketdi.core;

import com.freesolutions.pocket.pocketdi.utils.NullableOpResult;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;
import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class RequiredKey {

    private static final Comparator<RequiredKey> PRETTY_ORDERING =
        Comparator.<RequiredKey, Boolean>comparing(x -> x.property)
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.name))
            )
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.type != null ? x.type.getTypeName() : null))
            )
            .thenComparing(
                Comparator.nullsFirst(Comparator.comparing(x -> x.qualifier))
            );

    public final boolean property;
    @Nullable
    public final String name;
    @Nullable
    public final Type type;
    @Nullable
    public final String qualifier;

    @Nonnull
    public static Comparator<RequiredKey> prettyOrdering() {
        return PRETTY_ORDERING;
    }

    @Nonnull
    public static RequiredKey ofProperty(@Nonnull String name) {
        return new RequiredKey(true, name, null, null);
    }

    @Nonnull
    public static RequiredKey ofTypeOnly(@Nonnull Type type) {
        return new RequiredKey(false, null, type, null);
    }

    @Nonnull
    public static RequiredKey ofTypeQualified(@Nonnull Type type, @Nonnull String qualifier) {
        return new RequiredKey(false, null, type, qualifier);
    }

    @Nonnull
    public static RequiredKey ofTypeNamed(@Nonnull String name, @Nonnull Type type) {
        return new RequiredKey(false, name, type, null);
    }

    @Nonnull
    public static RequiredKey ofFull(
        boolean property,
        @Nullable String name,
        @Nullable Type type,
        @Nullable String qualifier
    ) {
        return new RequiredKey(property, name, type, qualifier);
    }

    @Nonnull
    public Class<?> javaType() {
        if (type == null) {
            return Object.class;
        }
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        }
        if (type instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) type).getRawType();
        }
        throw new IllegalStateException("must be impossible, see checks in constructor");
    }

    @Nonnull
    public BindingKey toBindingKey() {
        return BindingKey.ofFull(property, name, type, qualifier);
    }

    @Nonnull
    public NullableOpResult<BindingKey, List<BindingKey>> singleMatched(@Nonnull Iterable<BindingKey> bindingKeys) {
        BindingKey found = null;
        List<BindingKey> ambiguity = null;
        for (BindingKey bindingKey : bindingKeys) {
            if (match(bindingKey)) {
                if (ambiguity == null) {
                    if (found == null) {
                        found = bindingKey;
                    } else {
                        ambiguity = new ArrayList<>();
                        ambiguity.add(found);
                        found = null; //for correctness
                    }
                }
                if (ambiguity != null) {
                    ambiguity.add(bindingKey);
                }
            }
        }
        if (ambiguity == null) {
            return NullableOpResult.ofOk(found);
        }
        if (ambiguity.size() > 1) {
            ambiguity.sort(BindingKey.prettyOrdering());
        }
        return NullableOpResult.ofNotOk(ambiguity);
    }

    public boolean match(@Nonnull BindingKey bindingKey) {
        if (property != bindingKey.property) {
            return false;
        }
        if (bindingKey.property) {
            return checkNotNull(bindingKey.name).equals(checkNotNull(name));
        }
        if (name != null && (bindingKey.name == null || !bindingKey.name.equals(name))) {
            return false;
        }
        if (!checkNotNull(bindingKey.type).equals(checkNotNull(type))) {
            return false;
        }
        if (qualifier != null && (bindingKey.qualifier == null || !bindingKey.qualifier.equals(qualifier))) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequiredKey that = (RequiredKey) o;
        if (property != that.property) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return qualifier != null ? qualifier.equals(that.qualifier) : that.qualifier == null;
    }

    @Override
    public int hashCode() {
        int result = (property ? 1 : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (qualifier != null ? qualifier.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        if (property) {
            return "<req:prop:" + name + ">";
        }
        return "<req:bean" +
            (name != null ? ":" + name : "") +
            "|" + checkNotNull(type).getTypeName() +
            (qualifier != null ? "(" + qualifier + ")" : "") +
            ">";
    }

    private RequiredKey(
        boolean property,
        @Nullable String name,
        @Nullable Type type,
        @Nullable String qualifier
    ) {
        if (property) {
            checkArgument(name != null, "name must be present");
            checkArgument(type == null, "type must be not specified");
            checkArgument(qualifier == null, "qualifier must be not specified");
        }
        if (!property) {
            checkArgument(type != null, "type must be specified");
        }
        if (name != null) {
            checkArgument(isNameValid(name), "name \"%s\" is invalid", name);
        }
        if (type != null) {
            checkArgument(
                type instanceof Class<?> || type instanceof ParameterizedType,
                "type must be one of Class<?> or ParameterizedType"
            );
        }
        if (qualifier != null) {
            checkArgument(isQualifierValid(qualifier), "qualifier \"%s\" is invalid", qualifier);
        }
        this.property = property;
        this.name = name;
        this.type = type;
        this.qualifier = qualifier;
    }

    private static boolean isNameValid(@Nonnull String name) {
        return !name.isEmpty()
            && name.charAt(0) != '.'
            && name.charAt(name.length() - 1) != '.'
            && name.matches("[a-zA-Z0-9._-]+");
    }

    private static boolean isQualifierValid(@Nonnull String qualifier) {
        return !qualifier.isEmpty()
            && qualifier.charAt(0) != '.'
            && qualifier.charAt(qualifier.length() - 1) != '.'
            && qualifier.matches("[a-zA-Z0-9._-]+");
    }
}
