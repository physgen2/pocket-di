package com.freesolutions.pocket.pocketdi.core;

import com.freesolutions.pocket.pocketdi.annotations.Bind;
import com.freesolutions.pocket.pocketdi.annotations.Configuration;
import com.freesolutions.pocket.pocketdi.annotations.Manual;
import com.freesolutions.pocket.pocketdi.annotations.PropertySource;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import com.freesolutions.pocket.pocketdi.internal.*;
import com.freesolutions.pocket.pocketdi.internal.data.BindingDescriptor;
import com.freesolutions.pocket.pocketdi.internal.data.BindingError;
import com.freesolutions.pocket.pocketdi.internal.data.BindingParameter;
import com.freesolutions.pocket.pocketdi.internal.value.PropertyExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class Injector {
    private static final Logger LOGGER = LoggerFactory.getLogger(Injector.class);

    @Nonnull
    private final Injected.Mutable current;
    @Nonnull
    private final LinkedHashMap<BindingKey, BindingDescriptor> pendingBindings = new LinkedHashMap<>();
    private boolean defaultPropertySourceEnabled = false;
    private final List<InstanceAdjuster> instanceAdjusters = new ArrayList<>();

    //NOTE
    // General invariants:
    //  1. pendingBindings.keys * base.instances.keys = empty
    //  2. pendingBindings.keys + base.instances.keys == base.orders.keys

    @Nonnull
    public static Injector initial() {
        return new Injector(new Injected.Mutable());
    }

    @Nonnull
    public static Injector basedOn(@Nonnull Injected base) {
        return new Injector(new Injected.Mutable(base));
    }

    @Nonnull
    public Injector enableDefaultPropertySource() {
        this.defaultPropertySourceEnabled = true;
        return this;
    }

    @Nonnull
    public Injector addInstanceAdjuster(@Nonnull InstanceAdjuster instanceAdjuster) {
        instanceAdjusters.add(instanceAdjuster);
        return this;
    }

    @Nonnull
    public Injector bindProperties(@Nonnull Map<String, ?> properties) {
        properties.forEach(this::bindProperty);
        return this;
    }

    @Nonnull
    public Injector bindProperties(@Nonnull Map<String, ?> properties, boolean overwritePendingIfPresent) {
        properties.forEach((k, v) -> bindProperty(k, v, overwritePendingIfPresent));
        return this;
    }

    @Nonnull
    public Injector bindProperty(@Nonnull String name, @Nonnull Object value) {
        return bindProperty(name, value, true);
    }

    @Nonnull
    public Injector bindProperty(@Nonnull String name, @Nonnull Object value, boolean overwritePendingIfPresent) {
        PropertyValue propertyValue = PropertyValue.ofRawValueOrNull(value);
        if (propertyValue == null) {
            throw new DIException("Type of value is not compatible with property, value: " + value);
        }
        BindingKey bindingKey = BindingKey.ofProperty(name);
        failIfClash(bindingKey, true); //allow clash for pending property to be able to override it
        if (!overwritePendingIfPresent && pendingBindings.containsKey(bindingKey)) {
            //do nothing
            return this;
        }
        var bindingDescriptor = BindingDescriptor.forProperty(bindingKey, propertyValue);
        bindingDescriptor.parameters.forEach(this::registerPropertyExtensions);
        pendingBindings.put(bindingKey, bindingDescriptor);
        current.orders.put(bindingKey, Bind.ORDER_DEFAULT); //default order for properties
        registerSensitiveKeys(bindingKey, bindingDescriptor.parameters);
        return this;
    }

    @Nonnull
    public Injector bindInstance(@Nonnull Object instance) {
        return bindInstance(instance, Bind.ORDER_DEFAULT);
    }

    @Nonnull
    public Injector bindInstance(@Nonnull Object instance, int order) {
        return bindInstance(
            MetaExtractor.of(instance.getClass()).classBindingKey(),
            instance,
            order
        );
    }

    @Nonnull
    public Injector bindInstance(@Nonnull BindingKey bindingKey, @Nonnull Object instance) {
        return bindInstance(bindingKey, instance, Bind.ORDER_DEFAULT);
    }

    @Nonnull
    public Injector bindInstance(@Nonnull BindingKey bindingKey, @Nonnull InstanceProducer instanceProducer) {
        return bindInstance(bindingKey, instanceProducer, Bind.ORDER_DEFAULT);
    }

    @Nonnull
    public Injector bindInstance(@Nonnull BindingKey bindingKey, @Nonnull Object instance, int order) {
        return bindInstance(bindingKey, () -> instance, order);
    }

    @Nonnull
    public Injector bindInstance(@Nonnull BindingKey bindingKey, @Nonnull InstanceProducer instanceProducer, int order) {
        if (bindingKey.property) {
            throw new DIException("Can't bind " + bindingKey + ", because properties can't be bound as instances, use another binding method");
        }
        failIfClash(bindingKey, false);
        var bindingDescriptor = BindingDescriptor.forInstanceProducer(bindingKey, instanceProducer);
        bindingDescriptor.parameters.forEach(this::registerPropertyExtensions);
        pendingBindings.put(bindingKey, bindingDescriptor);
        current.orders.put(bindingKey, order);
        registerSensitiveKeys(bindingKey, bindingDescriptor.parameters);
        return this;
    }

    @Nonnull
    public Injector bindSingleton(@Nonnull Class<?> cls) {
        return bindSingleton(cls, Bind.ORDER_DEFAULT);
    }

    @Nonnull
    public Injector bindSingleton(@Nonnull Class<?> cls, int order) {
        return bindSingleton(
            MetaExtractor.of(cls).classBindingKey(),
            cls,
            order
        );
    }

    @Nonnull
    public Injector bindSingleton(@Nonnull BindingKey bindingKey, @Nonnull Class<?> cls) {
        return bindSingleton(bindingKey, cls, Bind.ORDER_DEFAULT);
    }

    @Nonnull
    public Injector bindSingleton(@Nonnull BindingKey bindingKey, @Nonnull Class<?> cls, int order) {
        if (bindingKey.property) {
            throw new DIException("Can't bind " + bindingKey + ", because singleton of class " + cls + " is not property");
        }
        checkNotNull(bindingKey.type);
        if (!(bindingKey.type instanceof Class)) {//singleton binding type must be always class type
            throw new DIException("Can't bind " + bindingKey + ", because singleton of class " + cls + " can't be bound to non-class type " + bindingKey.type);
        }
        if (!((Class<?>) bindingKey.type).isAssignableFrom(cls)) {
            throw new DIException("Can't bind " + bindingKey + ", because singleton of class " + cls + " can't be instance of binding key's type " + bindingKey.type);
        }
        if (!MetaExtractor.of(cls).hasSinglePublicConstructor()) {
            throw new DIException("Can't bind " + bindingKey + " with singleton of class " + cls.getTypeName() + ", because it has no single public constructor");
        }
        failIfClash(bindingKey, false);
        var bindingDescriptor = BindingDescriptor.forSingletonClass(bindingKey, cls, false);
        bindingDescriptor.parameters.forEach(this::registerPropertyExtensions);
        pendingBindings.put(bindingKey, bindingDescriptor);
        current.orders.put(bindingKey, order);
        registerSensitiveKeys(bindingKey, bindingDescriptor.parameters);
        return this;
    }

    @Nonnull
    public Injector bindConfiguration(@Nonnull Class<?> configurationClass) {

        //gather property sources
        PropertySource[] propertySources = configurationClass.getAnnotationsByType(PropertySource.class);
        if (propertySources.length > 0) {
            if (current.propertySourcesGathered) {
                throw new DIException(
                    "Failed configuration class " + configurationClass.getSimpleName() + " to gather @PropertySource-s," +
                        "already was gathered in another configuration class"
                );
            }
            bindProperties(PropertiesExtractor.gatherProperties(propertySources), false);
            current.propertySourcesGathered = true;
        }

        //bind configuration class itself
        BindingKey bindingKey = MetaExtractor.of(configurationClass).classBindingKey();
        checkState(!bindingKey.property);
        if (!MetaExtractor.of(configurationClass).hasSinglePublicConstructor()) {
            throw new DIException("Can't bind configuration class " + configurationClass.getTypeName() + ", because it has no single public constructor");
        }
        failIfClash(bindingKey, false);
        //NOTE
        // Always disable adjusting (proxies, wrappers, ...) for configuration classes, because
        // we must be able to make real calls of configuration methods during binding process.
        var bindingDescriptor = BindingDescriptor.forSingletonClass(bindingKey, configurationClass, true);
        bindingDescriptor.parameters.forEach(this::registerPropertyExtensions);
        pendingBindings.put(bindingKey, bindingDescriptor);
        current.orders.put(bindingKey, Bind.ORDER_DEFAULT); //default order for configurations
        registerSensitiveKeys(bindingKey, bindingDescriptor.parameters);

        //bind necessary methods
        for (Method method : configurationClass.getMethods()) {
            Bind[] binds = method.getAnnotationsByType(Bind.class);
            if (binds.length > 1) {
                throw new DIException("Method " + method + " has more than one @Bind in configuration " + configurationClass);
            }
            if (binds.length == 1) {
                bindMethod(method, binds[0].order());
            }
        }

        //run manuals
        for (Method method : configurationClass.getMethods()) {
            Manual[] manuals = method.getAnnotationsByType(Manual.class);
            if (manuals.length > 1) {
                throw new DIException("Method " + method + " has more than one @Manual in configuration " + configurationClass);
            }
            if (manuals.length == 1) {
                processManual(method);
            }
        }

        return this;
    }

    @Nonnull
    public Injector bindFromPackageScan(@Nonnull Class<?> pipette) {
        return bindFromPackageScan(pipette.getClassLoader(), pipette.getPackage().getName());
    }

    @Nonnull
    public Injector bindFromPackageScan(@Nonnull ClassLoader classLoader, @Nonnull String packageName) {
        Set<String> classNames = PackageScanner.scanClasses(classLoader, packageName);
        for (String className : classNames) {

            //load class
            Class<?> cls;
            try {
                cls = Class.forName(className);
            } catch (Throwable ignored) {
                //NOTE:
                // Just ignore, because scanning must work silently, and
                // probably some classes can't be load for some reasons.
                continue;
            }

            //detect as configuration
            Configuration[] configurations = cls.getAnnotationsByType(Configuration.class);
            if (configurations.length > 1) {
                throw new DIException("Class " + cls + " has more than one @Configuration");
            }
            if (configurations.length == 1) {
                Bind[] binds = cls.getAnnotationsByType(Bind.class);
                if (binds.length > 0) {
                    throw new DIException("Class " + cls + " with @Configuration can't be mixed with @Bind");
                }
                bindConfiguration(cls);
                continue;
            }

            //detect as singleton
            Bind[] binds = cls.getAnnotationsByType(Bind.class);
            if (binds.length > 1) {
                throw new DIException("Class " + cls + " has more than one @Bind");
            }
            if (binds.length == 1) {
                bindSingleton(cls, binds[0].order());
                continue;
            }

            //NOTE: do nothing more with class, seems this is simple class without any relation to DI
        }
        return this;
    }

    @Nonnull
    public Injected cook() {

        //gather default property source if required
        if (defaultPropertySourceEnabled && !current.propertySourcesGathered) {
            bindProperties(PropertiesExtractor.gatherPropertiesFromDefaultSource(), false);
            current.propertySourcesGathered = true;
        }

        //print pending properties
        printPrettyPendingProperties();

        //plan
        var bindingPlan = BindingPlanner.plan(
            pendingBindings.values(), //bindingDescriptors
            current.instances.keySet() //instanceKeys
        );
        if (!bindingPlan.errors.isEmpty()) {
            StringBuilder sb = new StringBuilder("\n\nThere are binding problems: ");
            for (BindingError error : bindingPlan.errors) {
                sb.append('\n').append(error.describe());
            }
            throw new DIException(sb.toString());
        }

        //process
        Map<BindingKey, Object> processResult = BindingProcessor.process(
            bindingPlan.commands,
            bindingPlan.lazies,
            current.sensitiveKeys,
            instanceAdjusters,
            new HashMap<>(current.instances) //inOutInstances
        );

        //update state: safe operations
        processResult.forEach((bindingKey, instance) -> {

            //remove from pending
            boolean ok = pendingBindings.remove(bindingKey) != null;
            checkState(ok);

            //add to instances
            current.instances.put(bindingKey, instance);
        });

        //check state
        checkState(pendingBindings.isEmpty());

        //result
        return current.fix();
    }

    private void printPrettyPendingProperties() {

        //retrieve properties only
        LinkedHashMap<String, String> properties = new LinkedHashMap<>();
        pendingBindings.values().stream()
            .filter(x -> x.ownerKey.property)
            .forEach(x ->
                properties.put(
                    checkNotNull(x.ownerKey.name),
                    checkNotNull(x.propertyValue).value
                )
            );

        //sensitive property names
        Set<String> sensitivePropertyNames = current.sensitiveKeys.stream()
            .filter(x -> x.property)
            .map(x -> checkNotNull(x.name))
            .collect(Collectors.toSet());

        //build lines
        StringBuilder sb = new StringBuilder("\nNext properties will be created:\n");
        sb.append("--------------------------------------------------\n");
        if (!properties.isEmpty()) {
            int nameMaxLength = properties.keySet().stream().mapToInt(String::length).max().orElse(0);
            String format = "%-" + (nameMaxLength + 1) + "s";
            properties.forEach((name, value) -> sb
                .append(String.format(format, name))
                .append(": ")
                .append(!sensitivePropertyNames.contains(name) ? PropertyValue.javaEscape(value) : "***hidden***")
                .append('\n')
            );
        } else {
            sb.append("No property values\n");
        }
        sb.append("--------------------------------------------------");

        //log
        LOGGER.info(sb.toString());
    }

    private void failIfClash(BindingKey bindingKey, boolean allowClashForPendingProperty) {
        current.instances.keySet().stream()
            .filter(x -> x.clash(bindingKey))
            .findFirst()
            .ifPresent(clashedKey -> {
                throw new DIException("Binding " + bindingKey + " clashes with " + clashedKey + ", that is already present");
            });
        pendingBindings.keySet().stream()
            .filter(x -> (!allowClashForPendingProperty || !x.property) && x.clash(bindingKey))
            .findFirst()
            .ifPresent(clashedKey -> {
                throw new DIException("Binding " + bindingKey + " clashes with " + clashedKey + ", that is already pending");
            });
    }

    private void bindMethod(@Nonnull Method method, int order) {
        BindingKey bindingKey = MetaExtractor.of(method.getDeclaringClass()).methodBindingKey(method);
        checkState(!bindingKey.property);
        failIfClash(bindingKey, false);
        var bindingDescriptor = BindingDescriptor.forConfigurationMethod(bindingKey, method);
        bindingDescriptor.parameters.forEach(this::registerPropertyExtensions);
        pendingBindings.put(bindingKey, bindingDescriptor);
        current.orders.put(bindingKey, order);
        registerSensitiveKeys(bindingKey, bindingDescriptor.parameters);
    }

    private void registerPropertyExtensions(@Nonnull BindingParameter bindingParameter) {
        PropertyExtension propertyExtension = bindingParameter.propertyExtension;
        if (propertyExtension != null) {
            checkState(bindingParameter.parameterKey.property); //because only properties can have extensions
            registerPropertyExtension(
                bindingParameter.parameterKey.toBindingKey(),
                propertyExtension
            );
        }
    }

    private void registerPropertyExtension(@Nonnull BindingKey propertyKey, @Nonnull PropertyExtension propertyExtension) {
        checkArgument(propertyKey.property);
        PropertyExtension existingExtension = current.propertyExtensions.get(propertyKey);
        if (existingExtension != null) { //met earlier?
            if (existingExtension.equals(propertyExtension)) { //duplicated as expected
                return;
            }
            throw new DIException(
                "Another extension have already present for property" +
                    ", property key: " + propertyKey +
                    ", existing extension: " + existingExtension +
                    ", new extension: " + propertyExtension
            );
        }

        //NOTE: property with this extension is new

        //check that property hasn't present yet
        if (current.instances.containsKey(propertyKey)) {
            throw new DIException(
                "Property is first time met with extension, but it have already present earlier without any extension" +
                    ", property key: " + propertyKey +
                    ", new extension: " + propertyExtension
            );
        }

        //auto-bind property if value present
        var value = propertyExtension.externalValueOrNull();
        if (value != null) {
            bindProperty(checkNotNull(propertyKey.name), value, true); //with overwriting if pending present
        }

        //register new property extension
        current.propertyExtensions.put(propertyKey, propertyExtension);
    }

    private void registerSensitiveKeys(@Nonnull BindingKey ownerKey, @Nonnull List<BindingParameter> bindingParameters) {
        bindingParameters.forEach(bindingParameter -> {
            if (bindingParameter.sensitive) {
                RequiredKey parameterKey = bindingParameter.parameterKey;
                if (current.sensitiveParameterKeys.add(parameterKey)) {
                    current.instances.keySet().stream()
                        .filter(parameterKey::match)
                        .forEach(current.sensitiveKeys::add);
                    pendingBindings.keySet().stream()
                        .filter(parameterKey::match)
                        .forEach(current.sensitiveKeys::add);
                }
            }
        });
        if (current.sensitiveParameterKeys.stream().anyMatch(parameterKey -> parameterKey.match(ownerKey))) {
            current.sensitiveKeys.add(ownerKey);
        }
    }

    private void processManual(@Nonnull Method method) {
        if (!Modifier.isPublic(method.getModifiers()) || !Modifier.isStatic(method.getModifiers())) {
            throw new DIException("Manual method " + method + " must be public-static");
        }
        if (!method.getReturnType().equals(Void.TYPE)) {
            throw new DIException("Manual method " + method + " must be void");
        }
        if (method.getParameterCount() != 1) {
            throw new DIException("Manual method " + method + " has invalid arguments count");
        }
        if (method.getParameterTypes()[0] != Injector.class) {
            throw new DIException("Manual method " + method + " has invalid argument type");
        }
        try {
            method.invoke(null, this);
        } catch (Throwable t) {
            throw new DIException("Error while processing manual method " + method, t);
        }
    }

    private Injector(@Nonnull Injected.Mutable current) {
        this.current = current;
    }
}
