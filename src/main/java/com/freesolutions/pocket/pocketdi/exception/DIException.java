package com.freesolutions.pocket.pocketdi.exception;

import javax.annotation.Nonnull;

/**
 * @author Stanislau Mirzayeu
 */
public class DIException extends RuntimeException {

    public DIException(@Nonnull String message) {
        super(message);
    }

    public DIException(@Nonnull Throwable t) {
        super(t);
    }

    public DIException(@Nonnull String message, @Nonnull Throwable cause) {
        super(message, cause);
    }
}
