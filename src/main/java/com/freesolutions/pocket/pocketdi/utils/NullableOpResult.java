package com.freesolutions.pocket.pocketdi.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class NullableOpResult<V, E> implements OpResult<V, E> {

    private static final NullableOpResult<?, ?> OK_NULL = new NullableOpResult<>(null, null);

    @Nullable
    private final V value;
    @Nullable
    private final E error;

    @SuppressWarnings("unchecked")
    @Nullable
    public static <V, E> NullableOpResult<V, E> ofOkIfOk(@Nonnull NullableOpResult<V, ?> source) {
        return source.isOk() ? (NullableOpResult<V, E>) source : null;//direct cast due to error is null
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public static <V, E> NullableOpResult<V, E> ofNotOkIfNotOk(@Nonnull NullableOpResult<?, E> source) {
        return !source.isOk() ? (NullableOpResult<V, E>) source : null;//direct cast due to value is null
    }

    @SuppressWarnings("unchecked")
    @Nonnull
    public static <V, E> NullableOpResult<V, E> ofOkNull() {
        return (NullableOpResult<V, E>) OK_NULL;
    }

    @Nonnull
    public static <V, E> NullableOpResult<V, E> ofOk(@Nullable V value) {
        return new NullableOpResult<>(value, null);
    }

    @Nonnull
    public static <V, E> NullableOpResult<V, E> ofNotOk(@Nonnull E error) {
        return new NullableOpResult<>(null, checkNotNull(error));
    }

    @Override
    public boolean isOk() {
        return error == null;
    }

    @Nullable
    @Override
    public V value() {
        checkState(isOk());
        return value;
    }

    @Nonnull
    @Override
    public E error() {
        //don't check ok, because this is equivalent to check error != null
        return checkNotNull(error);
    }

    @Nonnull
    @Override
    public NullableOpResult<V, E> mustOk() throws IllegalStateException {
        checkState(isOk());
        return this;
    }

    @Nonnull
    @Override
    public OpResult<V, E> mustNotOk() throws IllegalStateException {
        checkState(!isOk());
        return this;
    }

    protected NullableOpResult(@Nullable V value, @Nullable E error) {
        checkArgument(error == null || value == null);
        this.value = value;
        this.error = error;
    }
}
