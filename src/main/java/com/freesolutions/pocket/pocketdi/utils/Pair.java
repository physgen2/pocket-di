package com.freesolutions.pocket.pocketdi.utils;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@Immutable
public class Pair<L, R> {

    @Nonnull
    public final L left;
    @Nonnull
    public final R right;

    @Nonnull
    public static <L, R> Pair<L, R> of(@Nonnull Map.Entry<L, R> entry) {
        return new Pair<>(checkNotNull(entry.getKey()), checkNotNull(entry.getValue()));
    }

    @Nonnull
    public static <L, R> Pair<L, R> of(@Nonnull L left, @Nonnull R right) {
        return new Pair<>(left, right);
    }

    @Nonnull
    public L key() {
        return left;
    }

    @Nonnull
    public R value() {
        return right;
    }

    @Nonnull
    public <U> Pair<U, R> mapL(@Nonnull Function<? super L, ? extends U> mapper) {
        return Pair.of(checkNotNull(mapper.apply(left)), right);
    }

    @Nonnull
    public <U> Pair<L, U> mapR(@Nonnull Function<? super R, ? extends U> mapper) {
        return Pair.of(left, checkNotNull(mapper.apply(right)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return left.equals(pair.left) && right.equals(pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return "Pair<L: " + left + ", R: " + right + ">";
    }

    private Pair(@Nonnull L left, @Nonnull R right) {
        this.left = left;
        this.right = right;
    }
}
