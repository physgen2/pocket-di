package com.freesolutions.pocket.pocketdi.utils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.util.*;
import java.util.stream.Collectors;

import static com.freesolutions.pocket.pocketdi.utils.Utils.*;

/**
 * @author Stanislau Mirzayeu
 */
@NotThreadSafe
public class DependencyMesh {

    @NotThreadSafe
    private static class Node {

        public final int nodeId;

        @Nonnull
        public final List<Node> prev = new ArrayList<>(); //ordered by nodeId
        @Nonnull
        public final List<Node> next = new ArrayList<>(); //ordered by nodeId

        //help fields, see below usages within algorithms
        public int counter;
        public boolean mark;

        public Node(int nodeId) {
            this.nodeId = nodeId;
        }

        public void addPrev(@Nonnull Node node) {
            addSortedList(prev, node);
        }

        public void addNext(@Nonnull Node node) {
            addSortedList(next, node);
        }

        public void removePrev(int nodeId) {
            prev.removeIf(x -> x.nodeId == nodeId);
        }

        public void removeNext(int nodeId) {
            next.removeIf(x -> x.nodeId == nodeId);
        }

        public void removeLinks(boolean prevNNext) {
            if (prevNNext) {
                prev.forEach(x -> x.removeNext(nodeId));
                prev.clear();
            } else {
                next.forEach(x -> x.removePrev(nodeId));
                next.clear();
            }
        }

        private static void addSortedList(@Nonnull List<Node> list, @Nonnull Node node) {
            int position = 0;
            boolean replace = false;
            while (position < list.size()) {
                int curNodeId = list.get(position).nodeId;
                if (node.nodeId <= curNodeId) {
                    replace = (node.nodeId == curNodeId);
                    break;
                }
                position++;
            }
            if (replace) {
                list.set(position, node);
            } else {
                list.add(position, node);
            }
        }
    }

    private static final Random RANDOM = new Random();

    @Nonnull
    private final TreeMap<Integer, Node> nodes; //nodeId -> Node, ordered by nodeId

    public DependencyMesh() {
        this.nodes = new TreeMap<>();
    }

    @Nonnull
    public DependencyMesh copy() {
        DependencyMesh result = new DependencyMesh();
        nodes.values().forEach(node -> {
            Node resultNode = new Node(node.nodeId);
            resultNode.counter = node.counter;
            resultNode.mark = node.mark;
            result.nodes.put(node.nodeId, resultNode);
        });
        result.nodes.values().forEach(node ->
            checkNotNull(nodes.get(node.nodeId)).prev.forEach(childNode ->
                node.addPrev(checkNotNull(result.nodes.get(childNode.nodeId)))
            )
        );
        result.nodes.values().forEach(node ->
            checkNotNull(nodes.get(node.nodeId)).next.forEach(childNode ->
                node.addNext(checkNotNull(result.nodes.get(childNode.nodeId)))
            )
        );
        return result;
    }

    public boolean isEmpty() {
        return nodes.isEmpty();
    }

    @Nonnull
    public Set<Integer> nodeIds() {
        return Collections.unmodifiableSet(nodes.keySet());
    }

    public void ensureNode(int nodeId) {
        checkArgument(nodeId >= 0);
        if (!nodes.containsKey(nodeId)) {
            nodes.put(nodeId, new Node(nodeId));
        }
    }

    public void ensureLink(int fromNodeId, int toNodeId) {
        ensureNode(fromNodeId);
        ensureNode(toNodeId);
        Node fromNode = checkNotNull(nodes.get(fromNodeId));
        Node toNode = checkNotNull(nodes.get(toNodeId));
        toNode.addPrev(fromNode);
        fromNode.addNext(toNode);
    }

    public void removeNode(int nodeId) {
        Node node = nodes.remove(nodeId);
        if (node != null) {
            node.prev.forEach(x -> x.removeNext(nodeId));
            node.next.forEach(x -> x.removePrev(nodeId));
        }
    }

    public void removeLinks(int nodeId, boolean backwardNForward) {
        Node node = nodes.get(nodeId);
        if (node != null) {
            node.removeLinks(backwardNForward);
        }
    }

    public void removeFreeNodesInForwardDirection(@Nullable List<Integer> outRemovedNodeIds) {
        if (markFreeNodes(true, false, outRemovedNodeIds) > 0) {
            removeNodesByMark(true);
        }
    }

    @Nullable
    public Integer determineWeakestCycleNode(
        @Nullable Collection<Integer> includeNodeIds,
        @Nullable Collection<Integer> excludeNodeIds,
        int stepsPerNodeSq,
        @Nullable List<Integer> outUnbrokenCycleNodeIds
    ) {
        checkArgument(stepsPerNodeSq > 0);

        //short case
        if (nodes.isEmpty()) {
            return null;
        }

        //extract cycle mesh
        DependencyMesh cycleMesh = this;
        if (markFreeNodes(true, true, null) > 0) {
            cycleMesh = cycleMesh.copy();
            cycleMesh.removeNodesByMark(true);
        }

        //short case 2
        if (cycleMesh.nodes.isEmpty()) {
            return null;
        }

        int oneWalkSteps = stepsPerNodeSq * cycleMesh.nodes.size();
        checkState(oneWalkSteps > 0);

        //do random walks through cycle nodes (non-marked), starting from every node for every walk
        cycleMesh.nodes.values().forEach(x -> x.counter = 0); //clear counters
        for (Node node : cycleMesh.nodes.values()) {
            for (int i = 0; i < oneWalkSteps; i++) {
                node.counter++;
                checkState(!node.next.isEmpty()); //for cycle node at least one next link is always present
                node = node.next.get(RANDOM.nextInt(node.next.size()));
            }
        }

        //select node with max accumulated counter
        Integer resultNodeId = cycleMesh.nodes.values().stream()
            .filter(x -> includeNodeIds == null || includeNodeIds.contains(x.nodeId))
            .filter(x -> excludeNodeIds == null || !excludeNodeIds.contains(x.nodeId))
            .max(Comparator.comparingInt(x -> x.counter))
            .map(x -> x.nodeId)
            .orElse(null);

        //result
        if (resultNodeId == null) {
            if (outUnbrokenCycleNodeIds != null) {
                outUnbrokenCycleNodeIds.addAll(cycleMesh.nodeIds());
            }
        }
        return resultNodeId;
    }

    @Nonnull
    public String debugToString() {
        StringBuilder sb = new StringBuilder();
        sb.append(nodes.size()).append(" nodes\n");
        sb.append("----------\n");
        nodes.values().forEach(node -> {
            sb.append("node: ").append(node.nodeId);
            sb.append(", links: ").append(
                node.next.stream()
                    .map(x -> x.nodeId)
                    .collect(Collectors.toSet())
            );
            sb.append("\n");
        });
        sb.append("----------");
        return sb.toString();
    }

    private int markFreeNodes(
        boolean orForwardDirection,
        boolean orBackwardDirection,
        @Nullable List<Integer> outMarkedNodeIds
    ) {
        nodes.values().forEach(node -> node.mark = false); //clear all at start
        List<Node> curLayer = new ArrayList<>(nodes.values());
        List<Node> newLayer = new ArrayList<>();
        List<Node> tmpLayer = new ArrayList<>();
        int result = 0;
        while (!curLayer.isEmpty()) {
            newLayer.clear();
            for (Node node : curLayer) {
                if (!node.mark) {
                    tmpLayer.clear();
                    boolean prevPresent = false;
                    for (Node prevNode : node.prev) {
                        if (!prevNode.mark) {
                            prevPresent = true;
                            tmpLayer.add(prevNode);
                        }
                    }
                    boolean nextPresent = false;
                    for (Node nextNode : node.next) {
                        if (!nextNode.mark) {
                            nextPresent = true;
                            tmpLayer.add(nextNode);
                        }
                    }
                    if ((orForwardDirection && !prevPresent) || (orBackwardDirection && !nextPresent)) {
                        node.mark = true;
                        newLayer.addAll(tmpLayer);
                        if (outMarkedNodeIds != null) {
                            outMarkedNodeIds.add(node.nodeId);
                        }
                        result++;
                    }
                }
            }
            var tmp = curLayer;
            curLayer = newLayer;
            newLayer = tmp;
        }
        return result;
    }

    private void removeNodesByMark(boolean marked) {
        for (var i = nodes.values().iterator(); i.hasNext(); ) {
            Node node = i.next();
            if (node.mark == marked) {
                i.remove();
                node.prev.forEach(x -> x.removeNext(node.nodeId));
                node.next.forEach(x -> x.removePrev(node.nodeId));
            }
        }
    }
}
