package com.freesolutions.pocket.pocketdi.utils;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.exception.DIException;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.dynamic.scaffold.subclass.ConstructorStrategy;
import net.bytebuddy.implementation.InvocationHandlerAdapter;
import net.bytebuddy.implementation.MethodCall;
import net.bytebuddy.matcher.ElementMatchers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.NotThreadSafe;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Stanislau Mirzayeu
 */
public class LazyWrapping {

    @Immutable
    public static class LazyResult {

        @Nonnull
        public final Object proxy;
        @Nonnull
        public final Consumer<Object> setter;

        public LazyResult(
            @Nonnull Object proxy,
            @Nonnull Consumer<Object> setter
        ) {
            this.proxy = proxy;
            this.setter = setter;
        }
    }

    public static boolean isLazyPossible(@Nonnull Class<?> lazyClass) {
        return isProxyPossible(lazyClass);
    }

    @Nonnull
    public static LazyResult createLazy(
        @Nonnull Class<?> lazyClass,
        @Nonnull BindingKey ownerKey
    ) {
        if (!isProxyPossible(lazyClass)) {
            throw new DIException("Class " + lazyClass + " can't be wrapped with proxy");
        }
        LazyDelegator lazyDelegator = new LazyDelegator(lazyClass, ownerKey);
        return new LazyResult(
            makeProxy(
                lazyClass,
                minimalConstructor(lazyClass),
                lazyDelegator
            ),
            lazyDelegator::setDelegate
        );
    }

    @NotThreadSafe
    private static class LazyDelegator implements InvocationHandler {

        @Nonnull
        private final Class<?> lazyClass;
        @Nonnull
        private final BindingKey ownerKey;

        @Nullable
        private Object delegate;

        public LazyDelegator(
            @Nonnull Class<?> lazyClass,
            @Nonnull BindingKey ownerKey
        ) {
            this.lazyClass = lazyClass;
            this.ownerKey = ownerKey;
        }

        public void setDelegate(@Nonnull Object delegate) {
            this.delegate = delegate;
        }

        @Override
        public Object invoke(
            Object proxy,
            Method method,
            Object[] args
        ) throws Throwable {
            if (delegate == null) {
                if (method.getName().equals("toString") && method.getParameterCount() == 0) { //special case for toString()
                    return "<proxy:" + lazyClass.getSimpleName() + ">";
                }
                throw new IllegalStateException(
                    "Proxy delegate is not set yet" +
                        ", look for places with early calls of methods during injection phase" +
                        ", ownerKey: " + ownerKey
                );
            }
            return method.invoke(delegate, args);
        }
    }

    @Nonnull
    private static Object makeProxy(
        @Nonnull Class<?> proxyClass,
        @Nullable Constructor<?> constructor,
        @Nonnull InvocationHandler invocationHandler
    ) {
        try {
            var typeDefinition = new ByteBuddy()
                .subclass(
                    proxyClass,
                    constructor != null ?
                        ConstructorStrategy.Default.NO_CONSTRUCTORS :
                        ConstructorStrategy.Default.DEFAULT_CONSTRUCTOR
                )
                .method(ElementMatchers.any()).intercept(InvocationHandlerAdapter.of(invocationHandler));
            if (constructor != null) {
                typeDefinition = typeDefinition
                    .defineConstructor(Visibility.PUBLIC).intercept(
                        MethodCall.invoke(constructor)
                            .onSuper()
                            .with(defaultValuesFor(parameterTypes(constructor)))
                    );
            }
            return typeDefinition
                .make()
                .load(proxyClass.getClassLoader())
                .getLoaded()
                .getConstructor() //no-args constructor
                .newInstance();
        } catch (Throwable t) {
            throw new RuntimeException("Failed to create proxy for class " + proxyClass, t);
        }
    }

    private static boolean isProxyPossible(@Nonnull Class<?> cls) {
        if (cls.isPrimitive() || cls.isArray()) { //impossible to create proxy obviously for primitives & arrays
            return false;
        }
        if (cls.getPackageName().startsWith("java.") //don't allow any platform classes
            || cls.getPackageName().startsWith("jdk.")
            || cls.getPackageName().startsWith("sun.")
        ) {
            return false;
        }
        if (Modifier.isFinal(cls.getModifiers())) { //proxy classes impossible to derive from final classes
            return false;
        }
        if (cls.isInterface()) { //interfaces are the best choice and always allowed
            return true;
        }
        return Arrays.stream(cls.getFields()) //don't allow classed with public non-static fields
            .noneMatch(x ->
                Modifier.isPublic(x.getModifiers()) //public
                    && !Modifier.isStatic(x.getModifiers()) //non-static
            );
    }

    @Nullable
    private static Constructor<?> minimalConstructor(@Nonnull Class<?> cls) {
        return Arrays.stream(cls.getConstructors())
            .min(Comparator.comparingInt(Constructor::getParameterCount))
            .orElse(null);
    }

    @Nonnull
    private static List<Class<?>> parameterTypes(@Nonnull Constructor<?> constructor) {
        return Arrays.stream(constructor.getParameters())
            .map(Parameter::getType)
            .collect(Collectors.toList());
    }

    @Nonnull
    private static Object[] defaultValuesFor(@Nonnull List<Class<?>> types) {
        return types.stream()
            .map(x -> Array.get(Array.newInstance(x, 1), 0))
            .toArray();
    }
}
