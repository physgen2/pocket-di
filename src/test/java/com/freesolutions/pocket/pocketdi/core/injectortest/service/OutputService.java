package com.freesolutions.pocket.pocketdi.core.injectortest.service;

import com.freesolutions.pocket.pocketdi.annotations.Bind;
import com.freesolutions.pocket.pocketdi.annotations.Nonmandatory;
import com.freesolutions.pocket.pocketdi.annotations.Property;
import com.freesolutions.pocket.pocketdi.annotations.Qualifier;
import com.freesolutions.pocket.pocketdi.core.injectortest.utils.Generator;

import javax.annotation.Nullable;

@Bind
public class OutputService {

    private final double outputServiceProperty;
    private final Generator generator;
    private final InputService inputService;

    public OutputService(
        @Property("output.service.property") double outputServiceProperty,
        @Qualifier("secondary") Generator generator,
        InputService inputService,
        @Nonmandatory @Nullable NonExistingService nonExistingService,
        @Property("non.existing.property") @Nonmandatory @Nullable Boolean nonExistingProperty
    ) {
        this.outputServiceProperty = outputServiceProperty;
        this.generator = generator;
        this.inputService = inputService;
        System.out.println(
            "Called OutputService(" +
                "outputServiceProperty: " + outputServiceProperty +
                ", generator: " + generator +
                ", inputService: " + inputService +
                ", nonExistingService: " + nonExistingService +
                ", nonExistingProperty: " + nonExistingProperty +
                ")"
        );
        System.out.println(this);
    }

    public void callToInputService(int i) {
        System.out.println("OutputService.callToInputService(" + i + ")");
        if (i < 3) {
            inputService.callToOutputService(i + 1);
        }
    }

    @Override
    public String toString() {
        return "OutputService[" +
            "outputServiceProperty: " + outputServiceProperty +
            ", generator: " + generator +
            ", inputService: " + (inputService != null ? inputService.getClass() : null) +
            "]";
    }
}
