package com.freesolutions.pocket.pocketdi.core.injectortest.config;

import com.freesolutions.pocket.pocketdi.annotations.*;
import com.freesolutions.pocket.pocketdi.core.Injector;
import com.freesolutions.pocket.pocketdi.core.injectortest.utils.Generator;

import java.util.Collections;
import java.util.List;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkArgument;

@Configuration
@PropertySource("cp:com/freesolutions/pocket/pocketdi/core/injectortest/injector.test.app.properties")
public class AppConfiguration {

    public AppConfiguration(
        @Property("input.service.property") int inputServicePropertyInt,
        @Property("input.service.property") String inputServicePropertyStr,
        @Property("output.service.property") double outputServicePropertyDouble,
        @Property("output.service.property") String outputServicePropertyStr
    ) {
        checkArgument(inputServicePropertyInt == 146);
        checkArgument(inputServicePropertyStr.equals(" 146  "));
        checkArgument(outputServicePropertyDouble == 0.618 || outputServicePropertyDouble == 0.76393);
        checkArgument(outputServicePropertyStr.equals("  0.618") || outputServicePropertyStr.equals("0.76393"));
    }

    @Bind
    @Named("lists.year.prefix")
    public long listsYearPrefix() {
        return 2020L;
    }

    @Bind
    @Qualifier("primary")
    public Generator primaryGenerator(
        @Named("whitelist") List<String> whitelist,
        @Named("blacklist") List<String> blacklist
    ) {
        return new Generator("primary", whitelist, blacklist);
    }

    @Bind
    @Qualifier("secondary")
    public Generator secondaryGenerator(
        @Named("black_and_white_list") List<String> blackAndWhiteList
    ) {
        return new Generator("secondary", blackAndWhiteList, Collections.emptyList());
    }

    @Manual
    public static void manual1(Injector injector) {
        injector.bindProperty("manual.property.1", 555);
    }

    @Manual
    public static void manual2(Injector injector) {
        injector.bindProperty("manual.property.2", 666);
    }
}
