package com.freesolutions.pocket.pocketdi.core.injectortest;

import com.freesolutions.pocket.pocketdi.core.BindingKey;
import com.freesolutions.pocket.pocketdi.core.Injector;
import com.freesolutions.pocket.pocketdi.core.injectortest.service.InputService;
import org.junit.jupiter.api.Test;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.freesolutions.pocket.pocketdi.utils.Utils.checkNotNull;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class InjectorApplicationTest {

    @Test
    public void test() {

        System.setProperty("some_property_from_sysprop", "sysprop-value-123");
        System.setProperty("output_service_property_sysprop_123", Double.toString(0.76393));

        var injected = Injector.initial()
            .enableDefaultPropertySource()
            .bindFromPackageScan(InjectorApplicationTest.class)
            .cook();

        var instances = injected.instances();

        System.out.println("Bound instances: ");
        instances.forEach((bindingKey, instance) ->
            System.out.println("bindingKey: " + bindingKey + ", instance: " + instance)
        );

        checkNotNull((InputService) instances.get(BindingKey.ofTypeOnly(InputService.class))).callToOutputService(-2);
    }
}
