package com.freesolutions.pocket.pocketdi.core.injectortest.config;

import com.freesolutions.pocket.pocketdi.annotations.Bind;
import com.freesolutions.pocket.pocketdi.annotations.Configuration;
import com.freesolutions.pocket.pocketdi.annotations.Named;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class BlackAndWhiteConfiguration {

    @Bind
    @Named("blacklist")
    public List<String> blacklist(
        @Named("lists.year.prefix") long year
    ) {
        List<String> result = new ArrayList<>();
        result.add(year + "_black_element1");
        result.add(year + "_black_element2");
        result.add(year + "_black_element3");
        return result;
    }

    @Bind
    @Named("whitelist")
    public List<String> whitelist(
        @Named("lists.year.prefix") long year
    ) {
        List<String> result = new ArrayList<>();
        result.add(year + "_white_element1");
        result.add(year + "_white_element2");
        result.add(year + "_white_element3");
        return result;
    }

    @Bind
    @Named("black_and_white_list")
    public List<String> blackAndWhiteList(
        @Named("blacklist") List<String> blacklist,
        @Named("whitelist") List<String> whitelist
    ) {
        return new ArrayList<>() {{
            addAll(blacklist);
            addAll(whitelist);
        }};
    }
}
