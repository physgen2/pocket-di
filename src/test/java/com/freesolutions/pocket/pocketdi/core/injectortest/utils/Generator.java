package com.freesolutions.pocket.pocketdi.core.injectortest.utils;

import java.util.List;

public class Generator {

    private final String name;
    private final List<String> list1;
    private final List<String> list2;

    public Generator(
        String name,
        List<String> list1,
        List<String> list2
    ) {
        System.out.println("Called Generator(name: " + name + ", list1: " + list1 + ", list2: " + list2 + ")");
        this.name = name;
        this.list1 = list1;
        this.list2 = list2;
    }

    @Override
    public String toString() {
        return "Generator[name: " + name + ", list1: " + list1 + ", list2: " + list2 + "]";
    }
}
