package com.freesolutions.pocket.pocketdi.core.injectortest.service;

import com.freesolutions.pocket.pocketdi.annotations.Bind;
import com.freesolutions.pocket.pocketdi.annotations.Property;
import com.freesolutions.pocket.pocketdi.annotations.Sysprop;

@Bind
public class PropertyExtensionInjectionService {

    public PropertyExtensionInjectionService(
        @Property("some.property.from.sysprop") @Sysprop("some_property_from_sysprop") String somePropertyFromSysprop,
        @Property("output.service.property") @Sysprop("output_service_property_sysprop_123") double outputServiceProperty
    ) {
        //nothing
    }
}
