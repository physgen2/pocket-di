package com.freesolutions.pocket.pocketdi.core.injectortest.service;

import com.freesolutions.pocket.pocketdi.annotations.Bind;
import com.freesolutions.pocket.pocketdi.annotations.Property;
import com.freesolutions.pocket.pocketdi.annotations.Qualifier;
import com.freesolutions.pocket.pocketdi.core.injectortest.utils.Generator;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

@Bind
public class InputService {

    private static final Random RANDOM = new SecureRandom();

    private final List<Double> randomList = List.of(
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian()
    );

    private final int inputServiceProperty;
    private final Generator generator;
    private final OutputService outputService;

    public InputService(
        @Property("input.service.property") int inputServiceProperty,
        @Qualifier("primary") Generator generator,
        OutputService outputService
    ) {
        this.inputServiceProperty = inputServiceProperty;
        this.generator = generator;
        this.outputService = outputService;
        System.out.println(
            "Called InputService(" +
                "inputServiceProperty: " + inputServiceProperty +
                ", generator: " + generator +
                ", outputService: " + outputService +
                ", randomList: " + randomList +
                ")"
        );
        System.out.println(this);
    }

    public void callToOutputService(int i) {
        System.out.println("InputService.callToOutputService(" + i + ")");
        if (i < 3) {
            outputService.callToInputService(i + 1);
        }
    }

    @Override
    public String toString() {
        return "InputService[" +
            "inputServiceProperty: " + inputServiceProperty +
            ", generator: " + generator +
            ", outputService: " + (outputService != null ? outputService.getClass() : null) +
            ", randomList: " + randomList +
            "]";
    }
}
